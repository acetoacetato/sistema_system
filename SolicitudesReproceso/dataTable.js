
var tabla;

function buscarColumna(nombre, id){
            //console.log();
            tabla.column(id).search().draw()
    } 

$(document).ready( function () {
   
                
    // Se agrega un campo de selección al pie de cada columna seleccionable
    $('.select').each( function () {
        var nombreCol = $(this).text();
        $(this).html( '<select/>' );
    } );


   tabla = $('#tabla-reprocesos').DataTable( {
       //Carga los datos de la tabla con el JSON que retorna cargarBase.php
        ajax: {
            "url": "cargarBase.php",
            "type": "POST"
        },
       //Carga la tabla bastante más rápido
       "deferRender": true,
       //Muestra un aviso de 'procesando' cuando carga la tabla o búsquedas
       processing: true,
       // https://datatables.net/reference/option/dom
       dom: 'lBrtip',

       //Cantidad de sitios a mostrar por defecto
       pageLength: 8,
       //Se ordena por defecto con la columna 1 (COD_SITIO)
       "order": [[ 1, "desc" ]],
       //Largos permitidos en opción de mostrar n sitios
       "lengthMenu": [[8, 10, 25, 50, -1], [8, 10, 25, 50, "Todos"]],
       //se traducen los textos de la tabla
       language: {
            "sProcessing":     'Procesando...',
            "sLengthMenu":     "Mostrar _MENU_ solicitudes  ",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "No hay solicitudes de reproceso pendientes.",
            "sInfo":           "Mostrando solicitudes del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando 0 solicitudes coincidentes.",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ solicitudes)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": 'Cargando... Por favor espere',
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
           //Se especifican los detalles para mostrar los datos ocultos de la tabla
           //   en un modal.
        },responsive: {
              details: {
                  type: 'column',
                  target: 1,
                  }},
       //Se definen las tablas en el orden en que se desea que aparezcan
        columns: [
            {data: 'editar'},
            {data: 'POP'},
            {data: 'TIPO_SITIO'},
            {data: 'PROCESO'},
            {data: 'TOKEN'}
        ],
       //Se le da a todos los elementos la clase 'escribe'
        columnDefs: [{className: 'escribe', targets: '_all'},
                     //Se agrega un botón para editar el sitio
                     {targets: 0, defaultContent: '<button class="btn btn-light btn-xs editar">Editar Sitio</button>'},
                     {targets: 'SELECCIONABLE', className: 'select'}
                    ,],
       //Botones para limpiar los campos de búsqueda y recargar los datos de la tabla.
       buttons:    [
            //Botón para limpirar todos los filtros de búsqueda
            {
                text: 'Limpiar Campos<br><u>[shift + L]</u>',
                className: 'btn btn-default btn-xs',
                key:{
                   shiftKey: true,
                   key: 'l'
                },
                action: function(e, dt, node, config){
                    $('#tabla-reprocesos tfoot input').val('');
                    $('#tabla-reprocesos tfoot select').val(''); 
                    tabla.search( '' )
                        .columns().search( '' )
                        .draw();
                    
                }
            },
            //Botón para recargar los datos de la tabla, conservando los filtros puestos
            {
                text: 'Recargar Tabla',
                className: 'btn btn-default btn-xs',
                action: function(e, dt, node, config){
                    tabla.ajax.reload();
                }
            }
                    
        ],
        //Una vez se haya cargado todo, se agregan las opciones de los select de las columnas pertinentes.
        initComplete: function () {
            this.api().columns('.select').every( function () {
                var column = this;
                //console.log(column);

                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    if(d != null)
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );


        }


    });


       
        // Se agregan las funciones para filtrar la búsqueda en cada columna pertinente
    tabla.columns(".escribe").every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function (e) {
            
            //console.log(e.keyCode);
            if ( e.keyCode == 13 && that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
  
    
    //Función que se ejecuta al presionar el botón de editar de cada sitio
    //Abre una ventana emergente para la edición del sitio
    $('#tabla-reprocesos tbody').on( 'click', 'button.editar', function () {
        var data = tabla.row( $(this).parents('tr') ).data();
        var token = data['TOKEN'];
        var win = window.open("../aprobarReproceso.php" + "?token=" + token,'Reproceso',  "height="+ screen.availHeight +
                              ", width=" + screen.availWidth +", scrollbars=yes, titlebar=no");

        //Cuando la ventana se cierre, se recargará la tabla
        var timer = setInterval(function() { 
            if(win.closed) {
                clearInterval(timer);
                tabla.ajax.reload();
            }
        }, 1000);
        
    } );

});

