<?php

        header('Content-Type: application/json');
        try{
            $dsn = "mysql:host=localhost:3306;dbname=oocc";
            $dbh = new PDO($dsn, 'root', '');
        }catch(PDOException $e){
            die("Falló la conexión a la base de datos");
        }
        
        $sql_str = "SET NAMES 'utf8'";
        $stmt = $dbh->prepare($sql_str);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $sql_str = "SELECT * FROM oocc.solicitud_repro";



        //echo($sql_str);
        $stmt = $dbh->prepare($sql_str);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();  
        $datosBrutos = array();

        $i = 0;
        while($fila = $stmt->fetch()){  

            $datosBrutos['data'][$i]= $fila;
            $datosBrutos['data'][$i]['editar'] = null;
            $i++;
        }
        if(empty($datosBrutos)){
            echo '{
                "sEcho": 1,
                "iTotalRecords": "0",
                "iTotalDisplayRecords": "0",
                "aaData": []
            }';
            return;
        }

        echo json_encode($datosBrutos);
        ?>