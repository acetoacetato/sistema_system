<?php
    class mails{
        public static $firma = 'SG OOCC, El&eacute;ctricas y afines.';
        public function obtenerPHPMailer(){
            /**
             * This example shows settings to use when sending via Google's Gmail servers.
             */
            //SMTP needs accurate times, and the PHP time zone MUST be set
            //This should be done in your php.ini, but this is how to do it if you don't have access to that
            //date_default_timezone_set('Etc/UTC');
            require_once '../../PHPMailer-master/PHPMailerAutoload.php';
            //Create a new PHPMailer instance
            $mail = new PHPMailer;
            //Tell PHPMailer to use SMTP
            $mail->isSMTP();

            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //Enable SMTP debugging
            // 0 = off (for production use)
            // 1 = client messages
            // 2 = client and server messages
            $mail->SMTPDebug = 0;
            //Ask for HTML-friendly debug output
            //$mail->Debugoutput = 'html';
            //Set the hostname of the mail server
            //$mail->Host = 'smtp.gmail.com';
            //$mail->Host = 'smtprelay.entel.cl';
            //$mail->Host = '10.43.18.142';
            $mail->Host = 'smtprelay.entel.cl';

            //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
            //$mail->Port = 25;
            $mail->Port = 25;
            //Set the encryption system to use - ssl (deprecated) or tls
            $mail->SMTPSecure = 'tls';
            //$mail->SMTPSecure = 'ssl';
            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;
            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = "Entel\SIS-GDCI";
            //Password to use for SMTP authentication
            $mail->Password = "Inicio.2017#";

            //Set who the message is to be sent from
            $mail->setFrom('SIS-GDCI@entel.cl', 'OOMM Informa');
            //Set an alternative reply-to address
            //$mail->addReplyTo('replyto@example.com', 'First Last');
            //Set who the message is to be sent to

            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $mail->isHTML(true);

            return $mail;
        }

        function mandarMailRepro($token, $pop, $proceso, $tipoSitio){
            //require '../../PHPMailer-master/PHPMailerAutoload.php';

            $mail = self::obtenerPHPMailer();
            //Se llenan los receptores del mail
            $sql = "SELECT * FROM oocc.DISTRIBUCION_MAILS WHERE evento = 'solicitud_repro';";
            $result = mysql_query($sql);
            if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){

                    if(!    $row['copia']){
                        $mail->addAddress($row['mail'], $row['nombre']);
                    }else{
                        $mail->AddCC($row['mail']);
                    }
                }

            }

            $mail->Subject = "Solicitud REPROCESO [${pop}]";

            $tablaAux = "
                            <table border=1 style=border-collapse:collapse>
                                <tr bgcolor=#0000FF>
                                    <td align=center>
                                        <pre>
                                            <font color=#FFFFFF>POP</font>
                                        </pre>
                                    </td>
                                    <td align=center>
                                        <pre>
                                            <font color=#FFFFFF>Tipo Sitio</font>
                                        </pre>
                                    </td>
                                    <td align=center>
                                        <pre>
                                            <font color=#FFFFFF>Proceso Solicitante</font>
                                        </pre>
                                    </td>
                                    <td align=center>
                                        <pre>
                                            <font color=#FFFFFF>Week</font>
                                        </pre>
                                    </td>
                                    
                                    
                                </tr>
                                <tr>
                                    <td><pre>
                                        ${pop}
                                    </pre></td>
                                    <td><pre>
                                        ${tipoSitio}
                                    </pre></td>
                                    <td><pre>
                                        ${proceso}
                                    </pre></td>
                                </tr> </table>";

            $mail->Body = "<pre>Estimado:
                    El sitio <b>${pop}</b>[${tipoSitio}] ha emitido una solicitud de reproceso en ${proceso} de acuerdo al siguiente detalle:

                    <br>

                    ${tablaAux}
                    </pre>
                    <pre>
                    Para aprobar o denegar la solicitud, deber&aacute; ingresar al siguiente link:

                    link = http://servgdci_w71/gdci/Desarrollos/LiberacionOOMM/aprobarReproceso.php?token=${token}

                    <b>Nota: \"Este mail se genera de forma automatica, favor no responder\".</b>

                    Atte.
                    ". self::$firma .".

                    </pre>";

            return $mail->send();

        }

        function mandarMailLib($evento, $POP, $tipoSitio){
            $mail = self::obtenerPHPMailer();

            $sql = "SELECT * FROM oocc.DISTRIBUCION_MAILS WHERE evento = '" . $evento . "';";
            $result = mysql_query($sql);
            if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){

                    if(!$row['copia']){
                        $mail->addAddress($row['mail'], $row['nombre']);
                    }else{
                        $mail->AddCC($row['mail']);
                    }
                }

            }

            $mail->Subject = ($evento == 'IT')? "Sitio ${POP} POP IT":"Sitio ${POP} liberado por ${evento}";



            //COD_SITIO, NOMBRE_SITIO, COD_REGION, TIPO_SITIO, PROYECTO_DESPUES, OOMM
            $sqlSitio="select COD_SITIO, NOMBRE_SITIO, COD_REGION, TIPO_SITIO, ESTADO_PROYECTO, OOMM, APTO, IMPLEMENTACION_NRO from oocc.sitios_oomm WHERE COD_SITIO = '${POP}' AND TIPO_SITIO = '${tipoSitio}' AND (ENVIO_${evento} = 'No' OR ISNULL(ENVIO_${evento})) ;";

            $resultadoSitio = mysql_query($sqlSitio);
            if($resultadoSitio != false && mysql_num_rows($resultadoSitio) > 0){
                        $tablaAux = "
                        <table border=1 style=border-collapse:collapse>
                            <tr bgcolor=#0000FF>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>POP</font>
                                    </pre>
                                </td>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>Nombre Sitio</font>
                                    </pre>
                                </td>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>Region</font>
                                    </pre>
                                </td>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>Tipo Sitio</font>
                                    </pre>
                                </td>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>Estado Proyecto</font>
                                    </pre>
                                </td>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>OOMM</font>
                                    </pre>
                                </td>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>APTO</font>
                                    </pre>
                                </td>
                                <td align=center>
                                    <pre>
                                        <font color=#FFFFFF>IT</font>
                                    </pre>
                                </td>
                            </tr>";

                            $rowSitio = mysql_fetch_array($resultadoSitio);

                            $tablaAux .= "<tr>
                                            <td>
                                                <pre>
                                                    ${rowSitio['COD_SITIO']}
                                                </pre>
                                            </td>
                                            <td>
                                                <pre>
                                                    ${rowSitio['NOMBRE_SITIO']}
                                                </pre>
                                            </td>
                                            <td>
                                                <pre>
                                                    ${rowSitio['COD_REGION']}
                                                </pre>
                                            </td>
                                            <td>
                                                <pre>
                                                    ${rowSitio['TIPO_SITIO']}
                                                </pre>
                                            </td>
                                            <td>
                                                <pre>
                                                    ${rowSitio['ESTADO_PROYECTO']}
                                                </pre>
                                            </td>
                                            <td>
                                                <pre>
                                                    ${rowSitio['OOMM']}
                                                </pre>
                                            </td>
                                            <td>
                                                <pre>
                                                    ${rowSitio['APTO']}
                                                </pre>
                                            </td>
                                            <td>
                                                <pre>
                                                    ${rowSitio['IMPLEMENTACION_NRO']}
                                                </pre>
                                            </td>
                                        </tr>
                                    </table>";
                $mail->Body = "<pre>Estimados:
                    Para su informaci&oacute;n, se ha liberado  el sitio <b>${POP}<b>(${rowSitio['NOMBRE_SITIO']}) [${tipoSitio}] en ${evento} de acuerdo al siguiente detalle:

                    <br>

                    ${tablaAux}
                    </pre>
                    <pre>
                    Las condiciones de liberaci&oacute;n se encuentran detalladas en el aplicativo de OOMM. Para ver el contenido ingrese al siguiente link:

                    link = http://servgdci_w71
                    Usuario = ranco
                    Clave = Entel123


                    <b>Nota: \"Este mail se genera de forma automatica, favor no responder\".</b>

                    Atte.
                    " . self::$firma .".

                    </pre>
                    ";

                //echo $mail->Body;


                if(!$mail->send()){
                    echo "Mailer Error: " . $mail->ErrorInfo;
                    mysql_query("UPDATE oocc.sitios_ooomm SET ENVIO_${evento}='No' WHERE COD_SITIO = '${POP}' AND TIPO_SITIO = '${tipoSitio}'");
                }else{
                    echo "Se Ha mandado el mensaje!";
                    mysql_query("UPDATE oocc.sitios_ooomm SET ENVIO_${evento}='Si' WHERE COD_SITIO = '${POP}' AND TIPO_SITIO = '${tipoSitio}'");
                }

            }


        }


        function notificarReproceso($sitio){
            $mail = self::obtenerPHPMailer();
            $sql = "SELECT * FROM oocc.DISTRIBUCION_MAILS WHERE evento = '${sitio['PROCESO']}'; ";
            $result = mysql_query($sql);
            if(mysql_num_rows($result) < 0)
                throw new Exception('No hay mails a los que enviar el mensaje.');

            while($row = mysql_fetch_array($result)){

                if(!$row['copia']){
                    $mail->addAddress($row['mail'], $row['nombre']);
                }else{
                    $mail->AddCC($row['mail']);
                }
            }

            $mail->Subject = "Reproceso Aprobado [${sitio['POP']}]";
            $alias = logica::obtenerAlias($sitio['PROCESO']);
            $tablaAux = "
                            <table border=1 style=border-collapse:collapse>
                                <tr bgcolor=#0000FF>
                                    <td align=center>
                                        <pre>
                                            <font color=#FFFFFF>POP</font>
                                        </pre>
                                    </td>
                                    <td align=center>
                                        <pre>
                                            <font color=#FFFFFF>Tipo Sitio</font>
                                        </pre>
                                    </td>
                                    <td align=center>
                                        <pre>
                                            <font color=#FFFFFF>Proceso Solicitante</font>
                                        </pre>
                                    </td>
                                </tr>
                                <tr>
                                    <td><pre>
                                        ${sitio['POP']}
                                    </pre></td>
                                    <td><pre>
                                        ${sitio['TIPO_SITIO']}
                                    </pre></td>
                                    <td><pre>
                                        ${alias}
                                    </pre></td>
                                </tr> </table>";
            //include_once('logica.php');
            
            $mail->Body = "<pre>Estimado:
                    El proceso ${alias} de <b>${sitio['POP']}</b>[${sitio['TIPO_SITIO']}] ha pasado a estar en reproceso de acuerdo al siguiente detalle:

                    <br>

                    ${tablaAux}
                    </pre>
                    <pre>
                    Si considera que esto afecta su proceso, solicitar reproceso en el siguiente link:

                    link = http://servgdci_w71/

                    <b>Nota: \"Este mail se genera de forma automatica, favor no responder\".</b>

                    Atte.
                    ". self::$firma .".

                    </pre>";

            if(!$mail->send()){
                    echo "Mailer Error: " . $mail->ErrorInfo;
                }else{
                    echo "Se ha enviado el mensaje!";
                }
            }
    }
    
        
        
?>