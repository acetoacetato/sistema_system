﻿<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>[ Editar Sitio_</title>
    
    <?php
        set_time_limit(120);
        header('Content-Type: text/html; charset=ISO-8859-1');
        session_start();
        if ( $_SESSION["estado"] != "ok")
            header ("Location: ../../Login.php");
        include_once('db.php');
        
        include_once('reproceso.php');
        mysql_query("SET NAMES 'utf8'");
        $usuario = $_SESSION['usuarioaux'];
        $con = mysql_connect("10.39.131.28","root","4321"); 
        mysql_select_db("gdci", $con);
        
        if(!isset($_GET['POP']) || !isset($_GET['TIPO_SITIO']))
            die("Error: No se ingresó POP o tipo de sitio.");
    
        $perfil = obtenerPerfil($usuario);
        $Admin = ($perfil == 'Admin');
        echo "Perfil: ($perfil)";
   
        //Obtiene el sitio dados los datos pasados por GET
        function obtenerSitio(){
            
            //Conexión a la base de datos
            //Se usa PDO para poder convertir el arreglo del fetch a un JSON
            try{
                $dsn = "mysql:host=localhost:3306;dbname=oocc";
                $dbh = new PDO($dsn, 'root', '');
            }catch(PDOException $e){
                die("Falló la conexión a la base de datos");
            }
            //Para que no hayan problemas con los acentos
            $sql_str = "SET NAMES 'utf8'";
            $stmt = $dbh->prepare($sql_str);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            //Se asume que existe un solo sitio con esa combinación de pop y tipo de sitio
            $sql_str = "SELECT * FROM oocc.sitios_oomm WHERE COD_SITIO = '${_GET['POP']}' AND TIPO_SITIO = '${_GET['TIPO_SITIO']}'";


            $stmt = $dbh->prepare($sql_str);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();  
            $datosBrutos = array();
            $sitio = $stmt->fetch();
            
            return $sitio;
        }
    
        //función que verifica si un campo específico puede ser modificado por el perfil del usuario que solicita modificaciones
        function existeEnDominio($campo){
            include_once 'logica.php';
            
            global $perfil;
            if($perfil == 'Admin'){
                return true;
            
            }
            if(isset(logica::$dominios[$perfil])){
                
                foreach(logica::$dominios[$perfil] as $campoDominio){
                    if($campo == $campoDominio){
                        //echo "(Si)<br>";
                        return true;
                    }
                }
            }
            //echo "(No)<br>";
            return false;
        }
        
        $sitio = obtenerSitio();
    
        //Esta función no se usa
        function valoresLib($estado){
            $valores = array('LIBERADO'=>'', 'NO LIBERADO'=>'', 'REPROCESO'=>'');
            
            if($estado != 'NO LIBERADO' || $estado != ''){
                $valores['NO LIBERADO'] = 'disabled';
            } 
            
            return $valores;
        }
    ?>
    
    <!--Bootstrap-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <script>
	function Pag_Anterior(pep){
		window.top.location.href = 'Index.php?Flag=1';
	}
    function actualizar(pep){
		window.top.location.href = '';
	}

    //A las fechas se les agrega un datePicker
    $( function() {
        $( ".fecha" ).datepicker({ dateFormat: 'yy-mm-dd', beforeShow: function(i) {console.log(i); if ($(i).attr('readonly')) { return false; } }});
    } );
    
    //Se ejecuta cada vez que cambia el estado (liberado/no liberado/reproceso)
    function actualizarFecha(nombre_campo, valor){
        var perfil = '<?php echo $perfil ?>';
        
        //Si el estado cambió a liberado
        if(valor == 'LIBERADO'){
            
            //Si la fecha no está vacía (0000-00-00) entonces se cambia a la fecha que está en el sistema
            if(sitio[nombre_campo]!= '0000-00-00'){
                console.log("entra");
                $('#' + nombre_campo).datepicker('setDate', sitio[nombre_campo]);
                
                //Si el usuario es un administrador, permite que se modifique la fecha
                if(perfil=='Admin')
                    $('#' + nombre_campo).removeAttr('readonly');
                return;
            }
            
            //Si la fecha está vacía, al liberar se sugiere la fecha de hoy y deja editarla
            $('#' + nombre_campo).datepicker('setDate', new Date('yy, mm-1, dd'));
            $('#' + nombre_campo).removeAttr('readonly');

        }else{
            //Si no se cambia el estado a liberado, entonces se deja la fecha en 0 y se quitan permisos de escritura
            if(valor != 'REPROCESO')
                $('#' + nombre_campo).val('0000-00-00');
            $('#' + nombre_campo).attr('readonly',true);
        }
    }

    //Función que se ejecuta cada vez que se usa el checkbox de COLOCALIZADOS
    function colo(){
        
        if($('#ES_COLO').prop('checked') == true){
            $('#COLO_HIDDEN').val('SI');
            $('#COLOCALIZADOS').val('NO LIBERADO');
            $('#COLO').val('NO LIBERADO');
            $('#COMENTARIO_COLO').removeAttr('readonly');
            $('#COLOCALIZADOS option').attr('disabled',false);
            $('#FECHA_LIB_COLO').val('0000-00-00');
        }else{
            $('#COLO_HIDDEN').val('NO');
            $('#COLOCALIZADOS').val('');
            $('#COLO').val('');
            $('#COLOCALIZADOS option:not(:selected)').attr('disabled',true);
            $('#COMENTARIO_COLO').val('');
            $('#COMENTARIO_COLO').val('').attr('readonly', true);
            $('#FECHA_LIB_COLO').val('0000-00-00');
        }
    }
        
    	
    

</script>
<style>
.texto {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
	height: 25px;
	text-align: left;
}
.textoc {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
	height: 25px;
	text-align: center;
}
.cabecera1 {
	text-align: center;
	alignment-adjust: central;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 14px;
	color: #FFF;
	font-weight: bold;
	background-color: #0154a0;
	height: 30px;
}
    .comentario{
    
    width: 100px;
    
}
</style>
</head>

<body>
    
    <?php
        require_once 'logica.php';
        $_SESSION['estadosCambiados'] = array();
        if(isset($_POST['submit'])){
            
            //Si es un perfil no administrador, y el proceso del que está encargado el perfil ya tiene una solicitud de reproceso,
            //   se le avisa que ya hay una solicitud pendiente y que debe contactar al administrador para resolverlo
            //   antes de poder realizar alguna modificación nueva
            foreach(logica::$procesos as $proceso){
                if( isset($proceso['encargado']) && $proceso['encargado'] == $perfil && solicitudPendiente($sitio['COD_SITIO'], $proceso['nombre'], $sitio['TIPO_SITIO'])){
                    echo '<h1>[ERROR]</h1><br><h2>Su proceso tiene una solicitud de reproceso pendiente.<br>Contacte a un administrador para resolver la solicitud.';
                    echo '<script>setTimeout(function () { window.close();}, 7000);</script>';
                    die();
                }
            }
            
            //Función que retorna la fecha más reciente entre las dependencias de un proceso
            function actualizarFecha($nombreProceso){
                if($nombreProceso = 'IT')
                    return $_POST['FECHA_LIB_IT'];
                //Si ya tiene una fecha establecida, se deja esa misma (por problema de fechas vacías rellenadas con 31-12-2018)
                /*if($_POST['FECHA_LIB_' . $nombreProceso] != '' && $_POST['FECHA_LIB_' . $nombreProceso] != '0000-00-00' )
                    return $POST['FECHA_LIB_' . $nombreProceso];*/
                
                $fechas = array('OOMM'=>array('FECHA_LIB_ING_OOCC', 'FECHA_LIB_ING_OOEE', 'FECHA_LIB_COLO'),
                                'APTO'=>array('FECHA_LIB_OOMM', 'FECHA_LIB_IMP_TX', 'FECHA_LIB_ING_RF'),
                                'IT'=>array('FECHA_LIB_IT')                                    
                          );
                $fecha_mayor = new DateTime('0000-00-00');
                foreach($fechas[$nombreProceso] as $fecha){
                    //echo $_POST[$fecha];
                    $fecha_aux =new DateTime($_POST[$fecha]);
                    
                    if($fecha_mayor < $fecha_aux)
                        $fecha_mayor = $fecha_aux;
                }
                
                return $fecha_mayor->format('Y-m-d');
            }
            
            //función que comprueba que las liberaciones cumplan con los requisitos de acuerdo al diagrama.
            //Retorna un arreglo resultado, con un booleano que confirma si es válido o no y un mensaje que 
            // explica qué es lo que no cumple.
            function comprobarEstados(){
                global $sitio;
                $cumpleEstado = false;
                //Arreglo que contiene los campos que no pueden estar vacíos
                $camposBasicos = array('COD_SITIO', 'NOMBRE_SITIO', 'COD_REGION', 'ESTADO_PROYECTO', 'TIPO_SITIO'); 
                
                //Arreglo de resultado a retornar
                $resultado = array('valido'=>true, 'mensaje'=>'Ingreso Fallido.');
                
                //Comprueba que todos los campos básicos tengan un dato
                foreach($camposBasicos as $campo){
                    if(!isset($_POST[$campo]) || $_POST[$campo] == ''){
                        $resultado['valido'] = false;
                        $resultado['mensaje'] .= 'Los campos superiores no pueden estar vacíos.' . $campo;
                        return $resultado;
                    }
                }
                //Separador para el mensaje de error, en caso que haya más de un campo que no cumpla con la lógica
                $coma = '';
                
                //Recorre cada proceso y confirma que sus dependencias estén liberadas.
                foreach(logica::$procesos as $proceso){              
                    $nombreProc = $proceso['nombre'];
                    if($proceso['tipo'] == 'proceso'){
                        //Si no hay cambios en el estado o se pidió un reproceso, se salta la verificación
                        if(($_POST[$nombreProc] == $sitio[$nombreProc]) || ($_POST[$nombreProc] != $sitio[$nombreProc] && $_POST[$nombreProc] == 'REPROCESO') )
                            continue;
                    }
                    
                    
                    //Si el proceso está liberado, hay que comprobar que cumpla con sus dependencias
                    //  , para lanzar error en caso de que no esté liberada alguna de ellas.
                    //Si el proceso es de tipo 'estado', entonces hay que comprobar las dependencias para cambiar el estado
                    if($_POST[$proceso['nombre']] == 'LIBERADO' || $proceso['tipo'] == 'estado'){  
                        $resultado['mensaje'] = "Ingreso Fallido. No se cumplen las condiciones para poder liberar el proceso.";
                        
                        //Si las dependencias son null, no se tiene que comprobar nada
                        if($proceso['dependencias']!=null){
                            if($proceso['tipo'] == 'estado'){
                                $_POST[$proceso['nombre']] = 'LIBERADO';
                                //echo $proceso['nombre'] . ' Antes: ' . $sitio[$proceso['nombre']] . '|| POST: ' . $_POST[$proceso['nombre']] . '<br>';
                            }
                            
                            //Se recorre cada dependencia
                            foreach($proceso['dependencias'] as $dependencia){
                                
                                //Si es un proceso, y no está liberado, entonces ya no se cumplen las condiciones de las dependencias
                                if($proceso['tipo'] == 'proceso' && $_POST[$dependencia] == 'NO LIBERADO' && $_POST[$dependencia] != ''){
                                
                                        $resultado['valido'] = false;
                                        $resultado['mensaje'] .=  $coma . $proceso['nombre'];
                                        $coma = ', ';
                                        //echo $proceso['nombre'];
                                    
                                        break;
 
                                }
                                //Si el proceso es un estado y la dependencia está en reproceso o no está liberado, se cambia el estado a no liberado
                                if($proceso['tipo'] == 'estado'){
                                    if($_POST[$dependencia] == 'NO LIBERADO' || ($_POST[$dependencia] == 'REPROCESO' && $_POST[$dependencia] == $sitio[$dependencia])){
                                        //echo '<br>' . $dependencia . ' es dependencia de ' . $proceso['nombre'] . '<br>';
                                    //Si es un estado, se cambia su valor a 'NO LIBERADO'
                                        $_POST[$proceso['nombre']] = 'NO LIBERADO';
                                        $_POST['FECHA_LIB_' . $proceso['nombre']] = '0000-00-00';
                                        break;
                                    }
                                    
                                    $_POST['FECHA_LIB_' . $proceso['nombre']] = actualizarFecha($proceso['nombre']);
                                    
                                    
                                }


                            }
                            
                            
                            

                            //Si el proceso cambió con la edición actual, se le agrega en el arreglo de estadosCambiados,
                            //   para su notificación al realizar el cambio.
                            if($proceso['tipo'] == 'estado' && $sitio[$proceso['nombre']] != 'LIBERADO' && $_POST[$proceso['nombre']] == 'LIBERADO'){
                                $_SESSION['estadosCambiados'][$proceso['nombre']]=$proceso['nombre'];
                                
                            }                        
                            
                        }
                        
                    }
                    
                    
                }
                return $resultado;
            }
            
            
            
            //Función que aplica los cambios, es llamada una vez se comprueba que los estados tienen sentido
            function editarSitio(){
                
                //Arreglo con los elementos a modificar.
                $arreglo_col = array('COD_SITIO', 'NOMBRE_SITIO', 'COD_REGION', 'TIPO_SITIO', 'PROYECTO', 'LIBERACION_OOCC',
                    'COMENTARIO_OOCC', 'LIBERACION_OOEE', 'COMENTARIO_OOEE', 'OOMM', 'ESTADO_PROYECTO','INGENIERIA_RF',
                    'COLOCALIZADOS', 'COMENTARIO_COLO', 'INGENIERIA_TX', 'IMPLEMENTACION_TX', 'IMPLEMENTACION_NRO',
                    'APTO', 'FECHA_LIB_ING_OOCC', 'FECHA_LIB_ING_OOEE', 'FECHA_LIB_OOMM', 'FECHA_LIB_ING_RF',
                    'FECHA_LIB_COLO', 'FECHA_LIB_IMP_TX', 'FECHA_LIB_IT', 'FECHA_LIB_APTO', 'ES_COLO', 'IT');
                
                //Arreglo que contiene los campos que podrían tener estado 'liberado', 'no liberado' o 'reproceso', usado para el envío de las solicitudes, evitando que se manden solicitudes de reproceso desde un comentario.
                $liberables = array('LIBERACION_OOCC'=>'', 'LIBERACION_OOEE'=>'', 'INGENIERIA_RF'=>'', 'COLOCALIZADOS'=>'', 'INGENIERIA_TX'=>'', 'IMPLEMENTACION_TX'=>'', 'IMPLEMENTACION_NRO'=>'');
                $str_1 = '';
                $coma = '';
                $sql_str = ''; 
                global $sitio;
                global $admin;
                foreach($arreglo_col as $col){
                    
                    //Si el perfil no puede editar el campo, se omite
                    if(!existeEnDominio($col))
                        continue;
                    
                    //Si es un reproceso nuevo, el usuario no es un admin y es un campo 'liberable'
                    if(isset($_POST[$col]) && $_POST[$col] == 'REPROCESO' && $sitio[$col] != 'REPROCESO' && !$admin && isset($liberables[$col])){
                        //MANDAR SOLICITUD DE REPROCESO
                        //---include_once('reproceso.php');
                        try{
                            mandarAviso($_POST['COD_SITIO'], $col, $_POST['TIPO_SITIO']);
                        } catch(Exception $e){
                            echo "<script> alert('" . $e->getMessage() ."');";
                            echo "window.close();</script>";
                            continue;
                        }
                        
                        echo 'Aviso enviado';
                        continue;
                    }
                    $col_limpia = '';
                    if(isset($_POST[$col])){
                        
                        //Se sanitiza los campos quitando las comillas simples.
                        $col_limpia = str_replace("'", "", $_POST[$col]);
                    }
                    
                    $str_1 .= $coma . $col . " = '" . $col_limpia . "'";
                    $coma = ', ';

                }
                if($str_1 == '')
                    die('no hay nada que modificar');
                //Se aplican los cambios al sitio con su id, ya que puede cambiar el tipo de sitio, o el POP, o cualquier campo
                $sitio = $GLOBALS['sitio'];
                $sql_str = "UPDATE oocc.sitios_oomm SET $str_1 WHERE ID = ${sitio['ID']} ";
                
                //echo $sql_str;
                mysql_query($sql_str);
                include_once('envioMails.php');
                //Recorre todos los procesos hasta el primer estado que no esté liberado, 
               $proc_anterior = null;
                include_once 'logica.php';
                foreach(logica::$procesos as $proceso){
                    if($proceso['tipo'] == 'estado'){
                        if(isset($_POST[$proceso['nombre']]) && $_POST[$proceso['nombre']] != 'LIBERADO'){
                            break;
                        }
                        
                        $proc_anterior = $proceso;
                    }
                }
                //Si hay un proceso que se haya liberado con el cambio, entonces se manda un mail notificando la liberación.
                if($proc_anterior!=null && isset($_SESSION['estadosCambiados'][$proc_anterior['nombre']]))
                    mails::mandarMailLib($proc_anterior['nombre'], $_POST['COD_SITIO'], $_POST['TIPO_SITIO']);
                
                
            }
            
            //Se verifica que los estados estén correctos
            $dato = comprobarEstados();
            if(!$dato['valido']){
                echo '<script> alert("' . comprobarEstados()['mensaje'] . '")</script>';
            }else{
                editarSitio();
            }
        }
    
    ?>


						

<form method="post" id="EDITA" action="#">
  <table border="1" align="center" style="border-collapse:collapse" width="50%">
    <tbody><tr>
      <td colspan="8" align="center" class="cabecera1">Editar Sitio</td>
    </tr>
    
        <tr>
            <input type="hidden" name="ES_COLO" id="COLO_HIDDEN" class="form-control" value="NO">
            <input type="hidden" name="COLOCALIZADOS" id="COLO" class="form-control" value="">

            
            <!--Fechas-->
            <input type="hidden" name="FECHA_LIB_IT" id="IT" class="form-control" style="width:98%" tabindex="1" value="0000-00-00">
            <input type="hidden" name="FECHA_LIB_APTO"  class="form-control" style="width:98%" tabindex="1" value="0000-00-00">
            <input type="hidden" name="FECHA_LIB_OOMM"  class="form-control" style="width:98%" tabindex="1" value="0000-00-00">
        </tr>
        <tr>
            <td class="texto" width="10%">POP:</td>
            <td width="10%"><input type="text" name="COD_SITIO" id="COD_SITIO" class="form-control" style="width:98%" tabindex="3" value="<?php echo $sitio['COD_SITIO'] ?>" readonly>
        </td>
    
      <td class="texto" width="10%">Región:</td>
      <td colspan="6" width="10%"><select name="COD_REGION" id="REGION" style="width:98%"><option value="0">.::Seleccione Región::.</option><option value="1">Región de Tarapaca</option><option value="2">Región de Antofagasta</option><option value="3">Región de Atacama</option><option value="4">Región de Coquimbo</option><option value="5">Región de Valparaíso</option><option value="6">Región del Libertador Gral Bernardo OHiggins</option><option value="7">Región del Maule</option><option value="8">Región del Bio-Bio</option><option value="9">Región de la Araucanía</option><option value="10">Región de los Lagos</option><option value="11">Región Aysén del Gral Carlos Ibáñez del Campo</option><option value="12">Región de Magallanes y de la Antártica Chilena</option><option value="13">Región Metropolitana</option><option value="14">Region de los Rios</option><option value="15">Region de Arica y Parinacota</option></select>                        	
                  </td>
      
    </tr>
    <tr>
      <td class="texto" width="10%">Nombre:</td>
      <td width="10%">
          <input type="text" name="NOMBRE_SITIO" id="NOMBRE_SITIO" class="form-control" style="width:98%" tabindex="3" value="<?php echo $sitio['NOMBRE_SITIO'] ?>" <?php echo ($Admin)? '':'readonly' ?>>
      <td colspan="8"></td>
    </tr>
    <tr>
      <td class="texto" width="10%">Proyecto:</td>
      <td width="10%" colspan=""><select name="PROYECTO" id="PROYECTO">
          <option value="">.::Seleccione Proyecto::.</option>
          <option value="CAPACIDAD" >COBERTURA</option>
          <option value="CAPACIDAD 2019">NUEVA COBERTURA</option>
          <option value="COBERTURA">CAPACIDAD</option>
        </select>
      </td>
	  <td class="texto" width="10%">Estado Proyecto:</td>
      <td width="10%" colspan="6">        
      			<select name="ESTADO_PROYECTO" id="ESTADO_PROYECTO" onchange="console.log('aaa');Reemplaza(this.value);">
                	<option value="" >.::Estado Proyecto::.</option>
                    <option value="VIGENTE" selected>VIGENTE</option>
                    <option value="SUSPENDIDO">SUSPENDIDO</option>
                    <option value="REACTIVADO">REACTIVADO</option>
                    <option value="REEMPLAZADO">REEMPLAZADO</option>
                   
                </select>                             
      </td>
    </tr>
    <tr>
    <td class="texto" width="10%">Tipo Sitio:</td>
    	<td colspan="8">
            <select name="TIPO_SITIO" id="TIPO_SITIO" style="width:98%">
            	<option value="" >.::Tipo Sitio::.</option>
                <option value="BAFI">BAFI</option>
                <option value="RANCO">RANCO</option>
            </select>                                              
        </td>
    </tr>
        <!--Estados-->
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>OOMM</b></td>
    </tr>
    <tr>
        <td colspan="10"><input name="OOMM" id="OOMM" class="form-control" style="width:100%; text-align: center" tabindex="1" value="LIBERADO" readonly></td>
    </tr>
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>APTO</b></td>
    </tr>
    <tr>
        <td colspan="10"><input name="APTO" id="APTO" class="form-control" style="width:100%; text-align: center" tabindex="1" value="LIBERADO" readonly></td>
    </tr>
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>IT</b></td>
    </tr>
    <tr>
        <td colspan="10"><input name="IT" id="IT" class="form-control" style="width:100%; text-align: center" tabindex="1" value="LIBERADO" readonly></td>    
    </tr>
    <!------------------ OOCC ------------------>
    <?php 
            $oculto = '';
            if($perfil != 'OOCC' && $perfil != 'Admin'){
                $oculto = 'hidden';
            }
                                   
            
    ?>
            <tr <?php echo $oculto ?>>
              <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>OOCC</b></td>
            </tr>
            <tr <?php echo $oculto ?>>
              <td class="texto" width="10%">Liberación OOCC:  </td>
              <td width="10%">
                  <select name="LIBERACION_OOCC" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_OOCC', this.value)">
                    <option value="LIBERADO">LIBERADO</option>
                    <option value="NO LIBERADO" <?PHP echo ($sitio['LIBERACION_OOCC'] != 'NO LIBERADO' && $perfil != 'Admin')? 'disabled':'' ?>>NO LIBERADO</option>
                    <option value="REPROCESO">REPROCESO</option>
                  </select>                     
              </td>

              <td class="texto" width="2%">Comentario: </td>
                <td width="100%" colspan="3" ><textarea type="text"  name="COMENTARIO_OOCC" id="COMENTARIO_OOCC" class="form-control comentario" tabindex="10" value=""></textarea></td>
                <td class="texto" width="10%">Fecha Lib:</td>
                <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_OOCC" name="FECHA_LIB_ING_OOCC" value="0000-00-00" ></td>
            </tr>

    <!------------------ OOEE ------------------>
    <?php 
            $oculto = '';
            if($perfil != 'OOEE' && $perfil != 'Admin'){
                $oculto = 'hidden';
            }
    ?>
        <tr <?php echo $oculto ?>>
          <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>OOEE</b></td>
        </tr>
        <tr <?php echo $oculto ?>> 
            <td class="texto" width="10%">Liberación OOEE: </td>
            <td width="10%">
              <select name="LIBERACION_OOEE" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_OOEE', this.value)">
                  <option value="LIBERADO">LIBERADO</option>
                  <option value="NO LIBERADO" <?PHP echo ($sitio['LIBERACION_OOEE'] != 'NO LIBERADO' && $perfil != 'Admin')? 'disabled':'' ?>>NO LIBERADO</option>
                  <option value="REPROCESO">REPROCESO</option>
                </select>   
            </td>
            <td class="texto" width="10%">Comentario: </td>
            <td width="10%" colspan="3">
                <textarea name="COMENTARIO_OOEE" id="CONDICION" class="form-control comentario" tabindex="10" value=""></textarea></td>
            <td class="texto" width="10%">Fecha Lib:</td>
            <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_OOEE" name="FECHA_LIB_ING_OOEE" value="0000-00-00"></td>
            </tr>

    <!------------------ COLO ------------------>
        <?php 
            $oculto = '';
            if($perfil != 'COLO' && $perfil != 'Admin'){
                $oculto = 'hidden';
            }
        ?>
            <tr <?php echo $oculto ?>>
              <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>COLOCALIZADO</b></td>
            </tr>
            <tr <?php echo $oculto ?>> 
                <td class="texto" width="10%">Colocalizado: <input type="checkbox" name="COLO" id="ES_COLO" value="NO" onchange="colo()"></td>
                <td width="10%">
                    <select name="COLOCALIZADOS" id="COLOCALIZADOS" style="width:100%" onchange="actualizarFecha('FECHA_LIB_COLO', this.value)">
                      <option value="LIBERADO">LIBERADO</option>
                      <option value="NO LIBERADO" <?PHP echo ($sitio['COLOCALIZADOS'] != 'NO LIBERADO' && $perfil != 'Admin')? 'disabled':'' ?>>NO LIBERADO</option>
                      <option value="REPROCESO">REPROCESO</option>
                    </select>
                </td>  
                <td class="texto" width="10%">Comentario: </td>
                <td width="10%" colspan="3">
                    <textarea name="COMENTARIO_COLO" id="COMENTARIO_COLO" class="form-control comentario" tabindex="10" value=""></textarea>
                </td>        
                <td class="texto" width="10%">Fecha Lib:</td>
                <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_COLO" name="FECHA_LIB_COLO" value="0000-00-00"></td>
            </tr>    
    <!------------------ INGENIERIA RF ------------------>
    <?php 
            $oculto = '';
            if($perfil != 'ING_RF' && $perfil != 'Admin'){
                $oculto = 'hidden';
            }
        ?>
        <tr <?php echo $oculto ?>>
          <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Ingenieria RF</b></td>
        </tr>
        <tr <?php echo $oculto ?>> 
            <td class="texto" width="10%">ING RF:</td>
            <td width="10%">
                <select name="INGENIERIA_RF" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_RF', this.value)">
                  <option value="LIBERADO">LIBERADO</option>
                  <option value="NO LIBERADO" <?PHP echo ($sitio['INGENIERIA_RF'] != 'NO LIBERADO' && $perfil != 'Admin')? 'disabled':'' ?>>NO LIBERADO</option>
                  <option value="REPROCESO">REPROCESO</option>
                </select>
            </td>  
            <td colspan="4"></td>
            <td class="texto" width="10%">Fecha Lib:</td>
            <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_RF" name="FECHA_LIB_ING_RF" value="0000-00-00"></td>
        </tr>    

    <!------------------ INGENIERIA TX ------------------>
    <?php 
            $oculto = '';
            if($perfil != 'ING_TX' && $perfil != 'Admin'){
                $oculto = 'hidden';
            }
        ?>
        <tr <?php echo $oculto ?>>
          <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Ingenieria TX</b></td>
        </tr>
        <tr <?php echo $oculto ?> > 
            <td class="texto" width="10%">Ingeniería TX:</td>
            <td width="10%">
                <select name="INGENIERIA_TX" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_TX', this.value)">
                  <option value="LIBERADO">LIBERADO</option>
                  <option value="NO LIBERADO" <?PHP echo ($sitio['INGENIERIA_TX'] != 'NO LIBERADO' && $perfil != 'Admin')? 'disabled':'' ?>>NO LIBERADO</option>
                  <option value="REPROCESO">REPROCESO</option>
                </select>
            </td>  
            <td colspan="4"></td>
            <td class="texto" width="10%">Fecha Lib:</td>
            <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_TX" name="FECHA_LIB_ING_TX" value="0000-00-00"></td>

        </tr>  
    <!------------------ IMPLEMENTACIÓN TX ------------------>
    <?php 
            $oculto = '';
            if($perfil != 'IMP_TX' && $perfil != 'Admin'){
                $oculto = 'hidden';
            }
        ?>
        <tr <?php echo $oculto ?>>
          <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Implementacion TX</b></td>
        </tr>
        <tr <?php echo $oculto ?>>
            <td class="texto" >Implementación TX:</td>
            <td width="10%">
                <select name="IMPLEMENTACION_TX" style="width:100%" onchange="actualizarFecha('FECHA_LIB_IMP_TX', this.value)">
                  <option value="LIBERADO">LIBERADO</option>
                  <option value="NO LIBERADO" <?PHP echo ($sitio['IMPLEMENTACION_TX'] != 'NO LIBERADO' && $perfil != 'Admin')? 'disabled':'' ?>>NO LIBERADO</option>
                  <option value="REPROCESO">REPROCESO</option>
                </select>
            </td>     
            <td colspan="4"></td>
            <td class="texto" width="10%">Fecha Lib:</td>
            <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_IMP_TX" name="FECHA_LIB_IMP_TX" value="0000-00-00"></td>
        </tr>
    <!------------------ IMPLEMENTACION NRO ------------------>
    <?php 
        $oculto = '';
        if($perfil != 'IMP_NRO' && $perfil != 'Admin'){
            $oculto = 'hidden';
        }
    ?>
        <tr <?php echo $oculto ?>>
          <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Implementación</b></td>
        </tr>
        <tr <?php echo $oculto ?>> 
            <td class="texto" width="10%">Imp:</td>
            <td width="10%">
                <select name="IMPLEMENTACION_NRO" style="width:100%" onchange="actualizarFecha('FECHA_LIB_IT', this.value)">
                  <option value="LIBERADO">LIBERADO</option>
                  <option value="NO LIBERADO" <?PHP echo ($sitio['IMPLEMENTACION_NRO'] != 'NO LIBERADO' && $perfil != 'Admin')? 'disabled':'' ?>>NO LIBERADO</option>
                  <option value="REPROCESO">REPROCESO</option>
                </select>
            </td>  
            <td colspan="4"></td>
            <td class="texto" width="10%">Fecha Lib:</td>
            <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_IT" name="FECHA_LIB_IT" value="0000-00-00" readonly></td>
        </tr>    

    <tr>
      <td colspan="8" height="80px" align="center"><input type="submit" name="submit" value="[ Aplicar Cambios_" style="background-color:#ff6702; font-family:'Trebuchet ms',Arial; font-size:13px; color:#FFF; width:125px; height:23px; display:inline-block;  border:0px;">
        &nbsp;
        <input type="button" name="Volver" value="[ Volver_" style="background-color:#ff6702; font-family:'Trebuchet ms',Arial; font-size:13px; color:#FFF; width:125px; height:23px; display:inline-block;  border:0px;" onclick="javascript:window.close();">
        <input type="button" name="limpiar" value="[ Limpiar_" style="background-color:#ff6702; font-family:'Trebuchet ms',Arial; font-size:13px; color:#FFF; width:125px; height:23px; display:inline-block;  border:0px;" onclick="javascript:actualizar('');"></td>
    </tr>

        
  </tbody></table>
</form>

</body>
    
    <?php
        //Se actualizan los datos del sitio a los últimos luego de las ediciones
        $sitio = obtenerSitio();

    ?>
    <script>
        //Arreglo con los datos del sitio ingresado, para rellenar el formulario
        var sitio = <?php echo json_encode($sitio)?>;
        $(document).ready( function (){
            
            //Arreglo con los datos del sitio ingresado, para rellenar el formulario
            var sitio = <?php echo json_encode($sitio)?>;
            
            //perfil del usuario conectado
            var perfil = '<?php echo $perfil ?>';

            //Se cambian los valores de cada elemento que esté en la tabla de id 'EDITA'
            $('#EDITA *').each(function(){
                function encuentraValor(id){
                    if(sitio[id] != null) return sitio[id];
                    else return null;

                } 
                var valor = encuentraValor(this.name);
                if(valor == null)
                    return;


                this.value = valor;

            });
            
            //Bloquea la edición de las fechas en caso de que estén ingresadas al cargar la página
            //  así se evita que hagan cambios en la fecha de liberación.
                $('#EDITA .fecha').each(function(){
                    console.log(this.id);
                    if(this.value != ''){
                        if(this.value == '0000-00-00' || perfil != 'Admin')
                            $('#' + this.id).attr('readonly', true);
                        else
                            $('#' + this.id).attr('readonly', false);
                            
                    }

                });

            //Se cambia el ckeck del colocalizado si es que es COLO
            var colo = ($('#COLO_HIDDEN').val() == 'SI')? true: false;
            $('#ES_COLO').attr('checked', colo);
            
            //Si no es colocalizado, se desactiva todo
            if(!colo){
                $('#COLOCALIZADOS option:not(:selected)').attr('disabled', true);
            }
            
            if(perfil != 'Admin'){
                var camposBasicos = ['REGION', 'PROYECTO', 'TIPO_SITIO', 'ESTADO_PROYECTO'];
                camposBasicos.forEach( function(valor, indice, array) {
                    console.log(valor);
                    console.log($('#' + valor))
                    $('#' + valor + ' option:not(:selected)').attr('disabled', true);
                });
                
            }
        });
        
        function Reemplaza(valor){	
		if(valor=='REEMPLAZADO'){
            var respuesta = confirm("Al reemplazar el sitio, el avance de este será reiniciado. ¿aceptar?\n En caso de querer deshacer los cambios.");
            var sitio = <?php echo json_encode($sitio)?>;
            if(respuesta){
                    location.href = "./AgregarSitio.php?ID=" + sitio['ID'] + "&reemplazo=reemplazar";
            }
            
            $('#ESTADO_PROYECTO').val(sitio['ESTADO_PROYECTO']);
            }
        }
    </script>

</html>