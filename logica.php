<?php

    /* Debido a la mala organización de la base de datos, no existe una forma general de referirse a los comentarios
     *    o fechas de liberación de cada proceso. Esta clase suple la falta de una organización en la base de datos.
     *
     */
	class logica{
        //Arreglo que guarda los procesos y sus dependencias
    //dependencias: procesos que deben estar liberados para que quien dependa de ellos pueda estar liberado
    //EL ORDEN DE LOS FACTORES ALTERA EL PRODUCTO: para agregar procesos, se debe seguir el orden de dependencia; las dependencias se deben encontrar antes en el arreglo
    /**
     * nombre = el nombre de la columna a verificar, se usa el mismo nombre en el sitio como en la base de datos.
     * alias= el nombre que se usará cuando se envíe el/los mails.
     * tipo = usado para verificar la lógica, su uso es explicado en AgregarSitio.php y EditarSitio.php.
     * dependencias = lista de elementos de los que depende la liberación de este proceso. Si no se cumplen todas,
     *                   no puede estar liberado.
     * encargado = Qué perfil es el que puede modificar el proceso, es usado en EditarSitio.php 
     */
        public static $procesos=array(
                    array('nombre'=>'INGENIERIA_TX', 'encargado'=>'ING_TX', 'alias'=>'Ingenier&iacute;a TX', 'tipo'=>'proceso', 'dependencias'=>null),
                    array('nombre'=>'LIBERACION_OOCC', 'encargado'=>'OOCC', 'alias'=>'OOCC', 'tipo'=>'proceso', 'dependencias'=>null),
                    array('nombre'=>'LIBERACION_OOEE', 'encargado'=>'OOEE', 'alias'=>'OOEE', 'tipo'=>'proceso', 'dependencias'=>null),
                    array('nombre'=>'INGENIERIA_RF', 'encargado'=>'ING_RF', 'alias'=>'Ingenier&iacute;a RF', 'tipo'=>'proceso', 'dependencias'=>null),
                    array('nombre'=>'COLOCALIZADOS', 'encargado'=>'COLO', 'alias'=>'Colocalizado', 'tipo'=>'proceso', 'dependencias'=>array('INGENIERIA_RF')),
                    array('nombre'=>'IMPLEMENTACION_TX', 'encargado'=>'IMP_TX', 'alias'=>'Implementaci&oacute;n TX', 'tipo'=>'proceso', 'dependencias'=>array('INGENIERIA_TX')),
                    array('nombre'=>'IMPLEMENTACION_NRO', 'encargado'=>'IMP_NRO', 'alias'=>'Implmentaci&oacute;n NRO', 'tipo'=>'proceso', 'dependencias'=>array('IMPLEMENTACION_TX', 'COLOCALIZADOS', 'LIBERACION_OOEE', 'LIBERACION_OOCC', 'INGENIERIA_RF')),
                    array('nombre'=>'OOMM', 'tipo'=>'estado', 'dependencias'=>array('COLOCALIZADOS', 'LIBERACION_OOEE', 'LIBERACION_OOCC')),
                    array('nombre'=>'APTO', 'tipo'=>'estado', 'dependencias'=>array('IMPLEMENTACION_TX', 'OOMM', 'INGENIERIA_RF')),
                    array('nombre'=>'IT', 'tipo'=>'estado', 'dependencias'=>array('IMPLEMENTACION_NRO', 'APTO')),		
        );
        
        
        /*  
         *  
         *  Es un Arreglo asociativo, las claves son los perfiles, y el arreglo asociado a cada uno con los nombres de las
         *      columnas que pueden ser modificados por ese perfil.
         */
        public static $dominios = array(
                'OOCC' => array('LIBERACION_OOCC', 'COMENTARIO_OOCC', 'FECHA_LIB_ING_OOCC','OOMM', 'FECHA_LIB_OOMM', 'APTO', 'FECHA_LIB_APTO', 'IT', 'FECHA_IT'),
                'OOEE' => array('LIBERACION_OOEE', 'COMENTARIO_OOEE', 'FECHA_LIB_ING_OOEE', 'OOMM', 'FECHA_LIB_OOMM', 'APTO', 'FECHA_LIB_APTO', 'IT', 'FECHA_IT'),
                'ING_TX' => array('INGENIERIA_TX', 'FECHA_LIB_ING_TX', 'OOMM', 'FECHA_LIB_OOMM', 'APTO', 'FECHA_LIB_APTO', 'IT', 'FECHA_IT'),
                'IMP_TX' =>array('IMPLEMENTACION_TX', 'FECHA_LIB_IMP_TX', 'OOMM', 'FECHA_LIB_OOMM', 'APTO', 'FECHA_LIB_APTO', 'IT', 'FECHA_IT'),
                'ING_RF' => array('INGENIERIA_RF', 'FECHA_LIB_ING_RF', 'OOMM', 'FECHA_LIB_OOMM', 'APTO', 'FECHA_LIB_APTO', 'IT', 'FECHA_IT'),
                'IMP_NRO' =>array('IMPLEMENTACION_NRO', 'OOMM', 'FECHA_LIB_OOMM', 'APTO', 'FECHA_LIB_APTO', 'IT', 'FECHA_LIB_IT')
        );

        //Función que retorna el alias dado un nombre
        public  static function obtenerAlias($nombre){
            foreach(self::$procesos as $proceso){
                if($proceso['tipo']=='estado')
                    continue;
                if($proceso['nombre'] == $nombre)
                    return $proceso['alias'];
            }
            return 'PROCESO INDEFINIDO';
        }

    }


?>

