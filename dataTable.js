
var tabla;

function buscarColumna(nombre, id){
            //console.log();
            tabla.column(id).search().draw()
    } 

$(document).ready( function () {
   
                
    // Se agrega un campo de selección al pie de cada columna seleccionable
    $('.select').each( function () {
        var nombreCol = $(this).text();
        $(this).html( '<select/>' );
    } );


   tabla = $('#tabla-busqueda').DataTable( {

       //Carga los datos de la tabla con el JSON que retorna cargarBase.php
        ajax: {
            "url": "cargarBase.php",
            "type": "POST"
        },
       //Carga la tabla bastante más rápido
       "deferRender": true,
       //Muestra un aviso de 'procesando' cuando carga la tabla o búsquedas
       processing: true,
       // https://datatables.net/reference/option/dom
       dom: 'lBrtip',

       //Cantidad de sitios a mostrar por defecto
       pageLength: 8,
       //Se ordena por defecto con la columna 1 (COD_SITIO)
       "order": [[ 2, "asc" ]],
       //Largos permitidos en opción de mostrar n sitios
       "lengthMenu": [[8, 10, 25, 50, -1], [8, 10, 25, 50, "Todos"]],
       //se traducen los textos de la tabla
       language: {
            "sProcessing":     'Procesando...',
            "sLengthMenu":     "Mostrar _MENU_ sitios  ",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando sitios del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando sitios del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ sitios)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": 'Cargando... Por favor espere',
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
           //Se especifican los detalles para mostrar los datos ocultos de la tabla
           //   en un modal.
        },responsive: {
              details: {
                  type: 'column',
                  target: 1,
                  //Para mostrar los detalles, usa un modal de bootstrap
                  display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return '<div style="text-align:center" id="titulo-modal">Detalles del sitio '+ data.COD_SITIO + "</div>";
                  }}),
                  //Función que genera el texto a mostrar
                  renderer: function ( api, rowIdx, columns ) {
                    var cont=0;

                        var arreglo = $.makeArray(columns);

                        //Encuentra y retorna el dato en la columna del titulo que recibe
                        function encontrarDato(titulo){
                            var dato;
                            for(var i in arreglo){
                                    dato = arreglo[i];
                                    if(dato.title == titulo)
                                            return dato.data
                            }
                            return '0';
                        }

                        //Tabla con los datos del detalle adjuntos
                        var data=`
                              <div class="container"><table id="tabla-modal" align="center" class="mx-auto m-4 table table-borderless">
                                    <tr>
                                        <td class="tag"><b>Nombre del Sitio:</b>
                                            </td><td><input type="text" class="solo" value="${encontrarDato('NOMBRE SITIO')}" disabled></td>
                                        <td class="tag"><b>Región:</b></td>
                                            <td><input type="text" class="solo" value="${encontrarDato('REGIÓN')}" disabled></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Pop Anterior</b></td>
                                            <td><input type="text" class="solo" value="${(encontrarDato('POP ANTIGUO')=='')? 'ninguno':encontrarDato('POP ANTIGUO')}" disabled></td>
                                        <td class="tag"><b>Fecha Apto</b></td>
                                            <td><input type="text" size="7" value="${encontrarDato('Fecha APTO')}" disabled>
                                                <input type="text" value="W${encontrarDato('W_APTO')}-${encontrarDato('Fecha APTO').split('-')[0]}" size="7" disabled></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Liberación OOMM:</b></td>
                                            <td><input type="text" class="solo" value="${encontrarDato('OOMM')}" disabled></td>
                                        <td class="tag"><b>Apto:</b></td>
                                            <td><input type="text" class="solo" value="${encontrarDato('APTO')}" disabled></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Fecha Lib RF:</b></td>
                                            <td><input type="text" value="${encontrarDato('Fecha Lib RF')}" size="7" disabled>
                                        <input type="text" value="W${encontrarDato('W_RF')}-${encontrarDato('Fecha Lib RF').split('-')[0]}" size="7" disabled></td>
                                        <td class="tag"><b>Fecha IT:</b></td>
                                            <td><input type="text" size="7" value="${encontrarDato('Fecha IT')}" disabled>
                                                <input type="text" value="W${encontrarDato('W_IT')}-${encontrarDato('Fecha IT').split('-')[0]}" size="7" disabled></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Fecha Lib Ing. TX:</b></td>
                                            <td><input type="text" size="7" value="${encontrarDato('Fecha Lib ING. TX')}" disabled>
                                                <input type="text" value="W${encontrarDato('W_ING_TX')}-${encontrarDato('Fecha Lib ING. TX').split('-')[0]}" size="7" disabled></td>
                                        <td class="tag"><b>Fecha Lib Impl. TX:</b></td>
                                            <td><input type="text" size="7"  value="${encontrarDato('Fecha lib Impl. TX')}" disabled>
                                            <input type="text" value="W${encontrarDato('W_IMP_TX')}-${encontrarDato('Fecha lib Impl. TX').split('-')[0]}" size="7" disabled></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Fecha OOMM:</b></td>
                                            <td><input type="text" size="7" value="${encontrarDato('Fecha OOMM')}" disabled>
                                                <input type="text" value="W${encontrarDato('W_OOMM')}-${encontrarDato('Fecha OOMM').split('-')[0]}" size="7" disabled></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Fecha Lib. OOCC:</b></td>
                                            <td><input type="text" size="7" value="${encontrarDato('Fecha Lib. OOCC')}" disabled>
                                                <input type="text" value="W${encontrarDato('W_OOCC')}-${encontrarDato('Fecha Lib. OOCC').split('-')[0]}" size="7" disabled></td>
                                        <td class="tag"><b>Comentario OOCC:</b></td>
                                            <td><textarea rows="6" cols="35" disabled>${encontrarDato('Comentario OOCC')} </textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Fecha Lib. OOEE:</b></td>
                                            <td><input type="text" size="7" value="${encontrarDato('Fecha Lib.OOEE')}" disabled>
                                                <input type="text" value="W${encontrarDato('W_OOEE')}-${encontrarDato('Fecha Lib.OOEE').split('-')[0]}" size="7" disabled></td>

                                                
                                        <td class="tag"><b>Comentario OOEE:</b></td>
                                            <td><textarea rows="6" cols="35" disabled>${encontrarDato('Comentario OOEE')} </textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="tag"><b>Fecha Lib. Colo:</b></td>
                                            <td><input type="text" size="7" value="${encontrarDato('Fecha Lib Colo')}" disabled>
                                                <input type="text" value="W${encontrarDato('W_COLO')}-${encontrarDato('Fecha Lib Colo').split('-')[0]}" size="7" disabled></td>
                                        <td class="tag"><b>Comentario Colo:</b></td>
                                            <td><textarea rows="6" cols="35" disabled>${encontrarDato('Comentario Colo')} </textarea></td>
                                    </tr>



                              </table></div>
                            ;`;


                    return data ? data:false;



      }}},
       //Se definen las tablas en el orden en que se desea que aparezcan
        columns: [
            {data: 'editar'}, 
            {data: 'detalles', className:'detalle'},
            { data: 'COD_SITIO'},
            { data: 'COD_SITIO_REEMPLAZO' },
            { data: 'NOMBRE_SITIO' },
            { data: 'COD_REGION' },
            { data: 'TIPO_SITIO' },
            { data: 'PROYECTO' },
            { data: 'ESTADO_PROYECTO' },
            { data: 'ES_COLO' },
            { data: 'OOMM'},
            { data: 'APTO' },
            { data: 'IMPLEMENTACION_NRO'},
            { data: 'COLOCALIZADOS' },
            { data: 'FECHA_LIB_COLO' },
            { data: 'COMENTARIO_COLO' },
            { data: 'INGENIERIA_RF' },
            { data: 'FECHA_LIB_ING_RF' },
            { data: 'LIBERACION_OOCC' },
            { data: 'FECHA_LIB_ING_OOCC' },
            { data: 'COMENTARIO_OOCC' },
            { data: 'LIBERACION_OOEE' },
            { data: 'FECHA_LIB_ING_OOEE' },
            { data: 'COMENTARIO_OOEE' },
            { data: 'INGENIERIA_TX' },
            { data: 'FECHA_LIB_ING_TX' },
            { data: 'IMPLEMENTACION_TX' },
            { data: 'FECHA_LIB_IMP_TX' },
            { data: 'FECHA_LIB_IT' },
            { data: 'FECHA_LIB_OOMM' },
            { data: 'FECHA_LIB_APTO' }, 
            { data: 'W_RF' },
            { data: 'W_IT' },
            { data: 'W_ING_TX' },
            { data: 'W_IMP_TX' },
            { data: 'W_OOCC' },
            { data: 'W_OOEE' },
            { data: 'W_COLO' },
            { data: 'W_OOMM' },
            { data: 'W_APTO' },


        ],
       //Se le da a todos los elementos la clase 'escribe'
        columnDefs: [{className: 'escribe', targets: '_all'},
                     //Se agrega un botón para editar el sitio
                     {targets: 0, defaultContent: '<button class="btn btn-light btn-xs editar">Editar Sitio</button>'},
                     {targets: 1, defaultContent: '<button type="button" class="btn btn-default btn-sm detalle"><span class="glyphicon glyphicon-plus"></span></button>'},
                     {targets: 'FECHA', defaultContent: '-'},
                     {targets: 'SELECCIONABLE', className: 'select'}
                    ,],
       //Botones para exportar a excel y limpiar los campos de búsqueda.
       buttons:    [{
           //Botón para exportar a excel los datos, se descargan los filtrados en caso de estarlos
           extend: 'excel',
           text:       'Descargar en Excel <br> <u>[Shift + E]</u>',
           className: 'btn btn-default btn-xs',
           key:{
               shiftKey: true,
               key: 'e'
           },
           exportOptions: {

                columns: '.EXPORTABLE'

            },
           action: function (e, dt, button, config) {           
               
               alert("La descarga puede demorarse varios segundos. Por favor presione aceptar y espere.");
               $.fn.dataTable.ext.buttons.excelHtml5.action
                    .call(dt.button(button), e, dt, button, config); 
            },
           filename: 'Exportacion-datos-', 
           customData: function (exceldata) {
               console.log("espere");
               exportExtension = 'Excel';
               return exceldata;
           }},

            //Botón para limpirar todos los filtros de búsqueda
            {
                text: 'Limpiar Campos<br><u>[shift + L]</u>',
                className: 'btn btn-default btn-xs',
                key:{
                   shiftKey: true,
                   key: 'l'
                },
                action: function(e, dt, node, config){
                    $('#tabla-busqueda tfoot input').val('');
                    $('#tabla-busqueda tfoot select').val(''); 
                    tabla.search( '' )
                        .columns().search( '' )
                        .draw();
                    
                }
            },
            //Botón para recargar los datos de la tabla, conservando los filtros puestos
            {
                text: 'Recargar Tabla',
                className: 'btn btn-default btn-xs',
                action: function(e, dt, node, config){
                    tabla.ajax.reload();
                }
            },
            //Botón para abrir una ventana con gráficos
            {
                text: 'Gráficos',
                className: 'btn btn-default btn-xs',
                action: function(e, dt, node, confit){
                    var win = window.open("./graficos/",'Gráficos"',  "height=420, width=600, scrollbars=yes, titlebar=no");
                }
            },
            {
                text: 'Agregar Sitio',
                className: 'btn btn-default btn-xs agregar',
                action: function(e, dt, node, confit){
                    window.top.location.href = './AgregarSitio.php';
                }
            },
            {
                text: 'Administrar Usuarios',
                className: 'btn btn-default btn-xs agregar',
                action: function(e, dt, node, confit){
                    window.top.location.href = './AdministrarUsuarios/';
                }
            },
            {
                text: 'Revisar Solicitudes <br> de Reproceso',
                className: 'btn btn-default btn-xs agregar',
                action: function(e, dt, node, confit){
                    window.top.location.href = './SolicitudesReproceso/';
                }
            }

                    
        ],
        //Una vez se haya cargado todo, se agregan las opciones de los select de las columnas pertinentes.
        initComplete: function () {
            this.api().columns('.select').every( function () {
                var column = this;
                //console.log(column);

                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    if(d != null)
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );


        }


    });


       
        // Se agregan las funciones para filtrar la búsqueda en cada columna pertinente
    tabla.columns(".escribe").every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function (e) {
            
            //console.log(e.keyCode);
            if ( e.keyCode == 13 && that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
  
    
    //Función que se ejecuta al presionar el botón de editar de cada sitio
    //Abre una ventana emergente para la edición del sitio
    $('#tabla-busqueda tbody').on( 'click', 'button.editar', function () {
        var data = tabla.row( $(this).parents('tr') ).data();
        var sitio = data['COD_SITIO'];
        var tipo_sitio = data['TIPO_SITIO'];
                var win = window.open("./EditarSitio.php" + "?POP=" +
                                      sitio + "&TIPO_SITIO=" + tipo_sitio,'Editar Sitio "' +
                                      data['NOMBRE_SITIO'] + '"    ',  "height="+ screen.availHeight +
                                      ", width=" + screen.availWidth +", scrollbars=yes, titlebar=no");

        //Cuando la ventana se cierre, se recargará la tabla
        var timer = setInterval(function() { 
            if(win.closed) {
                clearInterval(timer);
                tabla.ajax.reload();
            }
        }, 1000);
        
    } );

});

