SELECT * FROM oocc.distribucion_mails;
	--  PROYECTOS  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Erick Lemus', 'ELEMUS@ENTEL.CL', 0, 'INGENIERIA_RF');

INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Max Castillo', 'MSCastillo@entel.cl', 0, 'INGENIERIA_RF');

INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Hugo Hernandez', 'HHERNANDEZ@ENTEL.CL', 0, 'INGENIERIA_RF');

INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Francisco Nordenflycht', 'FNORDENFLYCHT@ENTEL.CL', 0, 'INGENIERIA_RF');

INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Juan José Olavarría', 'JOLAVARRIA@ENTEL.CL', 0, 'INGENIERIA_RF');
	--  DRAN  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Hernan Sanchez', 'HVSANCHEZ@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Carlos Pinto', 'CAPINTO@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Luis Ormazabal', 'LORMAZABAL@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('jose Luis Perez', 'JLPEREZ@ENTEL.CL', 0, 'INGENIERIA_RF');
	-- INGENIERIA TX  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Juan Guzman', 'JGuzman@entel.cl', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Rodney Sprohnle', 'RSPROHNLE@ENTEL.CL', 0, 'INGENIERIA_RF');

/* externos */		
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Jorge Barra', 'drc_jmbarra@entel.cl', 1, 'INGENIERIA_RF');


	--  IMPLEMENTACION TX  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Fabian Gonzalez Mendez', 'FAGONZALEZ@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('German Rojas', 'GPROJAS@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Jose Luis Gonzalez', 'JLGONZALEZ@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Jose Martinez', 'JFMartinez@entel.cl', 0, 'INGENIERIA_RF');
	
	--  OOCC  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Marco Quezada', 'MQUEZADA@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Jose Caceres', 'JJCACERES@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('David Sanhueza', 'DPSANHUEZA@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Sergio Meza', 'SRMEZA@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Marcela Rojas', 'MTROJAS@ENTEL.CL', 0, 'INGENIERIA_RF');
	--  OOEE  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Roberto Olea', 'ROlea@entel.cl', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Manuel Galaz', 'MGALAZ@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('David Astudillo', 'DLASTUDILLO@ENTEL.CL', 0, 'INGENIERIA_RF');
	--  COLOCALIZADOS  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Paola Gamelli', 'PGAMELLI@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Manuel Arancivbia', 'MVArancibia@entel.cl', 0, 'INGENIERIA_RF');
	--  CONTRATOS INMOBILIARIOS  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Pablo Contreras', 'PGCONTRERAS@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Carlos Oyarzun', 'COYARZUN@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Marcelo Aedo', 'MAEDO@ENTEL.CL', 0, 'INGENIERIA_RF');	
	--  INGENIERIA RF  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Patricio Quezada', 'PMQUEZADA@ENTEL.CL', 0, 'INGENIERIA_RF');
		
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Juan Acevedo', 'JAACEVEDO@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Enzo Aros', 'EAROS@ENTEL.CL', 0, 'INGENIERIA_RF');
	-- IMPLEMENTACION NRO  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Alejandro Guzman', 'ATGUZMAN@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Nicolas Fuenzalida', 'NFUENZALIDA@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Cristian Alucema', 'CALUCEMA@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Miguel Rivera', 'MVRIVERA@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Patricio Quezada', 'PMQUEZADA@ENTEL.CL', 0, 'INGENIERIA_RF');
	--  OTROS  --
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Leonardo Suazo', 'LSSUAZO@ENTEL.CL', 0, 'INGENIERIA_RF');
	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('Paulina Peña', 'PPENA@ENTEL.CL', 0, 'INGENIERIA_RF');
/*	
INSERT INTO oocc.distribucion_mails(nombre, mail, copia, evento)
	VALUES('', '', 0, 'INGENIERIA_RF');
*/	

SELECT * FROM oocc.distribucion_mails;