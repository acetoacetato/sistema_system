<html>
<head>
    <meta charset="UTF-8"><title>Gráficos Sistema</title>
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
    
    <?php
        try{
            $dsn = "mysql:host=localhost:3306;dbname=oocc";
            $dbh = new PDO($dsn, 'root', '');
        }catch(PDOException $e){
            die("Falló la conexión a la base de datos");
        }
        
        $sql_str = "SET NAMES 'utf8'";
        $stmt = $dbh->prepare($sql_str);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $sql_str = "SELECT COUNT(*) as cantidad FROM oocc.sitios_oomm WHERE IT = 'LIBERADO'";
        $stmt = $dbh->prepare($sql_str);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        $fila = $stmt->fetch();
        $cant_IT = $fila['cantidad'];
    
    ?>
<body>


<div id='container1'></div>
<div id='container2'></div>
<div id='container3'></div>
<div id='container4'></div>
<div id='container5'></div>
<div id='container6'></div>

</body>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>

<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src='jsontohighchart.js'></script>
<script>
   

    var max_y = <?php echo $cant_IT + 5; ?>;

/*
$('#container1').drawchart({
		URL: 'testserver.php?tipo_data=LIB_OOCC', // server that return JSON
		
		category: 'TIPO_SITIO',  //category variable (x axis label), use date field name for time series 
		seriesname: 'ESTADO_LIB', //series name variable
		variable: 'CANT_LIB',  //data variable 
		label: 'porcentaje de liberaciones', //label for variable　(optional)
		
		stack: 'percent',  //'normal', 'percent' or  null
		type: 'column',
		title: 'porcentaje de liberaciones de OOCC por RANCO/BAFI', 
		decimal : 0,   // decimal point
		
		datatable: 'no',  // to display data table	below the chart
		HCoptions: {
			//Highcharts Options here
			xAxis: {
				labels: {
					rotation: 0,
					y: 20
				}
			}
		}
});
    
$('#container2').drawchart({
		URL: 'testserver.php?tipo_data=IT_TOTAL_CAT', // server that return JSON
		
		category: 'TIPO_SITIO',  //category variable (x axis label), use date field name for time series 
		seriesname: 'ESTADO_LIB', //series name variable
		variable: 'CANT_LIB',  //data variable 
		label: 'Cantidad de liberaciones', //label for variable　(optional)
		
		stack: 'normal',  //'normal', 'percent' or  null
		type: 'column',
		title: 'cantidad de liberaciones IT por RANCO/BAFI', 
		decimal : 0,   // decimal point
		
		datatable: 'no',  // to display data table	below the chart
		HCoptions: {
			//Highcharts Options here
			xAxis: {
				labels: {
					rotation: 0,
					y: 20
				}
			}
		}
});
    
    */
    $('#container3').drawchart({
            URL: 'testserver.php?tipo_data=IT_TOTAL', // server that return JSON

            category: 'ESTADO_LIB',  //category variable (x axis label), use date field name for time series 
            variable: 'CANT_LIB',  //data variable 
            label: 'cantidad', //label for variable　(optional)

            type: 'pie',
            title: 'Porcentaje de liberaciones IT en el sistema', 
            decimal : 0,   // decimal point


            HCoptions: {
                //Highcharts Options here
                legend: {
                    maxHeight: '200'
                    }

            }
    });
    
    
    $('#container1').drawchart({
		URL: 'testserver.php?tipo_data=IT_SEMANAL', // server that return JSON
        
        //Para agregar una linea paralela al eje x
		linea_divisoria : 24,
		category: 'week',  //category variable (x axis label), use date field name for time series 
		//seriesname: 'ESTADO_LIB', //series name variable
		variable: ['cantidad', 'promedio', 'totalIT'],  //data variable 
		label: ['cantidad de liberaciones', 'promedio 4 semanas anteriores'], //label for variable　(optional)
		
		stack: null,  //'normal', 'percent' or  null
		type: ['column', 'line', 'line'],
		title: 'Cantidad semanal de liberaciones POP IT por RANCO/BAFI', 
		decimal : 0,   // decimal point
        
        ymin: [0, 0, 0],
        ymax: [max_y, max_y, max_y],
		
		datatable: 'no',  // to display data table	below the chart
		HCoptions: {
			//Highcharts Options here
			xAxis: {
				labels: {
					rotation: 0,
					y: 20
				}
			}
		}
});
/*
$('#container2').drawchart({
		URL: 'testserver.php', // server that return JSON
		
		category: 'date',  //category variable (x axis label), use date field name for time series 
		seriesname: 'product', //series name variable
		variable: ['quantity','revenue'],  //data variable 
		label: ['Sold Qty','Revenue($)'],

		stack: 'normal',  //'normal', 'percent' or  null
		type: ['column', 'line'],
		title: 'time series', 
		decimal : 0,   // decimal point
		
		ymin: [0,50],  //starting point for 1st and 2nd Y axis
		
		xaxistype: 'datetime',  //datetime, category
		interval: 'daily',   //for time series chart
		mindate: '2013-10-20', //yyyy-mm-dd
		maxdate: '2013-10-25',
		
		
});


$('#container3').drawchart({
		URL: 'testserver.php', // server that return JSON
		
		category: 'category',  //category variable (x axis label), use date field name for time series 
		variable: 'revenue',  //data variable 
		
		type: 'pie',
		title: 'pie chart', 
		decimal : 0,   // decimal point
		
		
		HCoptions: {
			//Highcharts Options here
			legend: {
				maxHeight: '200'
				}
			
		}
});


$('#container4').drawchart({
		URL: 'testserver.php', // server that return JSON
		
		category: 'product',  //category variable (x axis label), use date field name for time series 
		variable: ['quantity', 'revenue'],  //data variable ,  x, y
		label: ['Sold Qty','Revenue($)'],
		
		type: 'scatter',
		title: 'scatter chart', 
		decimal :  0,  // decimal point
		
		ymin: 0,  //min for first variable
		  // to display data table
		
});


$('#container5').drawchart({
		URL: 'testserver.php', // server that return JSON
		
		category: 'product',  //category variable (x axis label)
		variable: ['quantity', 'revenue', 'customer'],  //data variable  x, y, z
		
		type: 'scatter',
		title: 'Scatter bubble chart', 
		decimal :  0  
		
});



$('#container6').drawchart({
		URL: 'testserver.php', // server that return JSON
		
		category: 'product',  //category variable (x axis label)
		variable: ['quantity', 'revenue', 'customer'],  //data variable  x, y, z
		
		type: 'bubble',
		title: 'bubble chart', 
		decimal :  0, // decimal point
		ymin: [0,0],
		datatable: 'yes'  // to display data table
		
});
*/
</script>
</html>
