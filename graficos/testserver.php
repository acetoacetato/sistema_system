<?php

        header('Content-Type: application/json');

        
        try{
            $dsn = "mysql:host=localhost:3306;dbname=oocc";
            $dbh = new PDO($dsn, 'root', '');
        }catch(PDOException $e){
            die("Falló la conexión a la base de datos");
        }
        
        $sql_str = "SET NAMES 'utf8'";
            $stmt = $dbh->prepare($sql_str);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            $sql_str = "";
            switch($_GET['tipo_data']){
                    case 'LIB_OOCC':
                        $sql_str = "Select LIBERACION_OOCC as ESTADO_LIB, count(*) as CANT_LIB, TIPO_SITIO  from sitios_oomm group by LIBERACION_OOCC, TIPO_SITIO;";
                        break;
                    case 'IT_TOTAL_CAT':
                        $sql_str = "Select IMPLEMENTACION_NRO as ESTADO_LIB, count(*) as CANT_LIB, TIPO_SITIO  from sitios_oomm group by IMPLEMENTACION_NRO, TIPO_SITIO";
                        break;
                case 'IT_TOTAL':
                        $sql_str = "Select IMPLEMENTACION_NRO as ESTADO_LIB, count(*) as CANT_LIB from sitios_oomm group by IMPLEMENTACION_NRO;
";
                        break;
                case 'Tiempo_lib_implTX':
                        
                        $sql_str = " SELECT  *  FROM (SELECT 'Mayor o igual a 300' as categoria, count(*) as total FROM sitios_oomm where IMPLEMENTACION_TX = 'LIBERADO' AND INGENIERIA_TX = 'LIBERADO' and datediff(FECHA_LIB_IMP_TX, FECHA_LIB_ING_TX) >= 300) as algo
                        union all
                        SELECT  *  FROM (SELECT 'Menor a 300' as categoria, count(*) as total FROM sitios_oomm where IMPLEMENTACION_TX = 'LIBERADO' AND INGENIERIA_TX = 'LIBERADO' and datediff(FECHA_LIB_IMP_TX, FECHA_LIB_ING_TX) < 300) as algo";
                        break;   
                case 'IT_SEMANAL':
                        $sql_str = "select concat('W', week(FECHA_LIB_IT, 1) , '/', year(FECHA_LIB_IT)) AS week, count(*) AS cantidad from oocc.sitios_oomm where IMPLEMENTACION_NRO = 'LIBERADO' group by week order by YEARWEEK(FECHA_LIB_IT)";
                        break;
            }
            
            
            
            
            $stmt = $dbh->prepare($sql_str);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();  
            $datosBrutos = array();
            $totalIT = 0;
            $i = 0;
            while($fila = $stmt->fetch()){                  
                $datosBrutos[$i]= $fila;
                if($_GET['tipo_data'] == 'IT_SEMANAL'){
                    if($i>=3){
                        $prom = $datosBrutos[$i]['cantidad'] + $datosBrutos[$i-1]['cantidad'] + $datosBrutos[$i-2]['cantidad'] + $datosBrutos[$i-3]['cantidad'];
                        $datosBrutos[$i]['promedio'] = $prom/4;
                    }
                
                    $totalIT += $datosBrutos[$i]['cantidad'];
                    $datosBrutos[$i]['totalIT'] = $totalIT;
                    
                    
                }
                $i++;
            }
            echo json_encode($datosBrutos);
        ?>

