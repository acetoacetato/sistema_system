<?php

    //Crea la solicitud de reproceso y manda el mail para solicitar atención.
    function mandarAviso($POP, $proceso, $tipoSitio){
        
        //Librería para poder crear un token, el random de PHP7, para PHP5
        include_once './random_compat/lib/random.php';
        include_once('db.php');
        
        //La cantidad de carácteres del token
        $length = 15;
        
        //Se crea el token aleatorio
        $token = bin2hex(random_bytes($length));
        
        include_once('envioMails.php');
        
        $sql = "SELECT * FROM oocc.SOLICITUD_REPRO WHERE POP = '$POP' AND PROCESO = '$proceso' AND TIPO_SITIO = '$tipoSitio'";
        
        $result = mysql_query($sql);
        
        //Si ya hay una solicitud de reproceso en ese proceso, con ese pop y ese tipo de sitio, entonces se manda error.
        if($result != false && mysql_num_rows($result)>0)
            throw new Exception('[ERROR] Hay una solicitud de reproceso pendiente para este sitio.');
        $sql = "SELECT * FROM oocc.sitios_oomm WHERE COD_SITIO = '$POP' AND $proceso = 'REPROCESO' AND TIPO_SITIO = '$tipoSitio'";
        $result = mysql_query($sql);
        
        //Si el sitio ya se encuentra en reproceso, se manda error.
        if($result ==false || mysql_num_rows($result)>0)
            throw new Exception('El sitio ya está en reproceso.');
        
        
        //Se inserta la solicitud en la base de datos
        $sql = "INSERT INTO oocc.SOLICITUD_REPRO(TOKEN, POP, PROCESO, TIPO_SITIO) 
                        VALUES ('$token', '$POP', '$proceso', '$tipoSitio')";
        if(!mysql_query($sql)){
            echo $sql . '<br>' . mysql_error() . '<br>';
            
            throw new Exception('Fallo en el proceso de la solicitud, intente más tarde.1');
        }
        
        //Si falla el envío del mail, se elimina la solicitud y se lanza error.
        if(!mails::mandarMailRepro($token, $POP, $proceso, $tipoSitio)){
            $sql = "DELETE FROM SOLICITUD_REPRO WHERE TOKEN = '$token'";
            sql_query($sql);
            
            throw new Exception('Falló el proceso de la solicitud, intente más tarde.');
        }
        
        echo "Solicitud enviada!";
            
        
        
    }

    //Función que retorna si hay una solicitud de reproceso pendiente o no.
    function solicitudPendiente($POP, $proceso, $tipoSitio){
        $sql = "SELECT * FROM oocc.SOLICITUD_REPRO WHERE POP = '$POP' AND PROCESO = '$proceso' AND TIPO_SITIO = '$tipoSitio'";
        
        //echo $sql;
        $result = mysql_query($sql);
        
        if($result != false && mysql_num_rows($result)>0)
            return true;
    
        return false;
    }
    

?>