﻿<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>[ Agregar Sitio_</title>
    
    <?php
        set_time_limit(120);
        header('Content-Type: text/html; charset=ISO-8859-1');
        session_start();
        if ( $_SESSION["estado"] != "ok")
            header ("Location: ../../Login.php");
        include_once('db.php');
        $usuario = $_SESSION['usuarioaux'];
        $con = mysql_connect("10.39.131.28","root","4321"); 
        mysql_select_db("gdci", $con);
        
        $perfil = obtenerPerfil($usuario);
        
        //Si el usuario conectado no es un Administrador, entonces se le retorna a la página principal.
        if($perfil != 'Admin'){
            header("Location:./");
        }
        
        
        
    ?>
    
    <!--Bootstrap-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <script>
        
    //Retorna a la página principal.
	function Pag_Anterior(pep){
		window.top.location.href = 'Index.php?Flag=1';
	}
        
    //Actualiza la página.
    function actualizar(pep){
		window.top.location.href = '';
	}

    
    //Agrega un datePicker a cada input de fechas.
    $( function() {
        $( ".fecha" ).datepicker({ dateFormat: 'yy-mm-dd', beforeShow: function(i) {if ($(i).attr('readonly')) { return false; } }});
    } );

    //Se ejecuta cada vez que cambia el estado (liberado/no liberado/reproceso)
    function actualizarFecha(nombre_campo, valor){
        if(valor == 'LIBERADO'){
            $('#' + nombre_campo).datepicker('setDate', new Date('yy, mm-1, dd'));
            $('#' + nombre_campo).removeAttr('readonly');

        }else{
            $('#' + nombre_campo).val('0000-00-00');
            $('#' + nombre_campo).attr('readonly',true);

                

        }
    }
        
    //Función que se ejecuta cada vez que se activa/desactiva el checkbox de COLOCALIZADOS
    function colo(){
        if($('#ES_COLO').prop('checked') == true){
            $('#COLO_HIDDEN').val('SI');
            $('#COLOCALIZADOS').val('NO LIBERADO');
            $('#COLO').val('NO LIBERADO');
            $('#COMENTARIO_COLO').removeAttr('readonly');
            $('#COLOCALIZADOS option').attr('disabled',false);
        }else{
            $('#COLO_HIDDEN').val('NO');
            $('#COLOCALIZADOS').val('');
            $('#COLO').val('');
            $('#COLOCALIZADOS option:not(:selected)').attr('disabled',true);
            $('#COMENTARIO_COLO').val('');
            $('#COMENTARIO_COLO').val('').attr('readonly', true);
        }
    }
        
    //Se rellenan los datos al inicio
    $(document).ready( function (){
        $('#COLOCALIZADOS').val('').attr('readonly', true);
        $('#COMENTARIO_COLO').val('').attr('readonly', true);
        
        <?php if(isset($_POST['submit'])){ ?>
        
        var POST = <?php echo json_encode($_POST)?>;
       
        $('#AGREGA *').each(function(){
            function encuentraValor(id){
                if(POST[id] != null) return POST[id];
                else return null;

            } 
            var valor = encuentraValor(this.name);
            if(valor != null )
                this.value = valor;
        });
            
        <?php } ?>
        
    });

</script>
<style>
.texto {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
	height: 25px;
	text-align: left;
}
.textoc {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
	height: 25px;
	text-align: center;
}
.cabecera1 {
	text-align: center;
	alignment-adjust: central;
	font-family: Verdana, Geneva, sans-serif;
	font-size: 14px;
	color: #FFF;
	font-weight: bold;
	background-color: #0154a0;
	height: 30px;
}
    .comentario{
    
    width: 100px;
    
}
</style>
</head>

<body>
    
    <?php
        require_once 'logica.php';
    
        //Si se está reemplazando un sitio 
        if(isset($_GET['reemplazo'])){
            try{
                $dsn = "mysql:host=localhost:3306;dbname=oocc";
                $dbh = new PDO($dsn, 'root', '');
            }catch(PDOException $e){
                die("Falló la conexión a la base de datos");
            }

            $sql_str = "SET NAMES 'utf8'";
            $stmt = $dbh->prepare($sql_str);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            
            //Se obtiene el POP del sitio a reemplazar
            $sql_str = "SELECT COD_SITIO FROM oocc.sitios_oomm WHERE ID = '${_GET['ID']}'";
            $stmt = $dbh->prepare($sql_str);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            
            $pop = $stmt->fetch()['COD_SITIO'];
            
            //Se cambia el estado del sitio a REEMPLAZADO, para dejar el menú para agregar al nuevo sitio
            $sql_str = "UPDATE oocc.sitios_oomm SET ESTADO_PROYECTO = 'REEMPLAZADO' WHERE ID = '${_GET['ID']}'";
            
            $stmt = $dbh->prepare($sql_str);
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $stmt->execute();
            
            
            ?>
                <script>
                    
                    
                    $(document).ready( function (){
                        //Se cambia el POP anterior por el codigo del sitio que se está reemplazando    
                        $('#COD_SITIO_REEMPLAZO').val('<?php echo $pop ?>');
                        
                    });
                </script>
                
            <?php
        }
    
        //Si se está agregando el sitio
        if(isset($_POST['submit'])){
            
            //Función que comprueba que lo que se agrega tiene lógica según el diagrama de flujo.
            function comprobarEstados(){
                
                //Arreglo con los campos que no pueden estar vacíos
                $camposBasicos = array('COD_SITIO', 'NOMBRE_SITIO', 'COD_REGION', 'ESTADO_PROYECTO', 'TIPO_SITIO'); 
                                array('ESTADO_PROYECTO');
                
                //Arreglo con el resultado a retornar
                $resultado = array('valido'=>true, 'mensaje'=>'Ingreso Fallido.');
                
                //Comprueba que ningún campo básico esté vacío
                foreach($camposBasicos as $campo){
                    if(!isset($_POST[$campo]) || $_POST[$campo] == ''){
                        $resultado['valido'] = false;
                        $resultado['mensaje'] .= 'Los campos superiores no pueden estar vacíos.' . $campo;
                        return $resultado;
                    }
                }
                
                //Separador para mensaje de error
                $coma = '';
                
                //Se recorren todos los procesos en el arreglo de logica.php
                foreach(logica::$procesos as $proceso){                    
                    
                    //Si no tiene dependencias, el proceso se omite
                    if($proceso['dependencias']==null){
                        continue;
                    }
                    
                    //Si está liberado, se procede a confirmar que cumpla con las dependencias.
                    if($_POST[$proceso['nombre']] == 'LIBERADO'){                        
                        foreach($proceso['dependencias'] as $dependencia){
                            
                            //En caso de que no cumpla con al menos una de las dependencias
                            if($_POST[$dependencia] == 'NO LIBERADO'/* || $_POST[$dependencia] == 'REPROCESO'*/){
                                
                                //Si es proceso, se deja el resultado como fallido y se agrega el proceso al mensaje.
                                if($proceso['tipo'] == 'proceso'){
                                    $resultado['valido'] = false;
                                    $resultado['mensaje'] .=  $coma . $proceso['nombre'];
                                    $coma = ', ';
                                }else{
                                    //En caso de que sea un estado, se cambia el estado a 'NO LIBERADO'
                                    $_POST[$proceso['nombre']] = 'NO LIBERADO';
                                }
                                break;
                            }
                            //Si no llega a negarse, se cambia el estado a liberado, sólo por si acaso.
                            $_POST[$proceso['nombre']] = 'LIBERADO';
                        }
                    }
                    
                    
                }
                return $resultado;
            }
            
            //Función que agrega el sitio en la base de datos
            function agregarSitio(){
                
                //Arreglo con todos los campos del sitio.
                $arreglo_col = array('COD_SITIO', 'NOMBRE_SITIO', 'COD_SITIO_REEMPLAZO', 'COD_REGION', 'TIPO_SITIO', 'PROYECTO', 'LIBERACION_OOCC',
                    'COMENTARIO_OOCC', 'LIBERACION_OOEE', 'COMENTARIO_OOEE', 'OOMM', 'ESTADO_PROYECTO','INGENIERIA_RF',
                    'COLOCALIZADOS', 'COMENTARIO_COLO', 'INGENIERIA_TX', 'IMPLEMENTACION_TX', 'IMPLEMENTACION_NRO',
                    'APTO', 'FECHA_LIB_ING_OOCC', 'FECHA_LIB_ING_OOEE', 'FECHA_LIB_OOMM', 'FECHA_LIB_ING_RF',
                    'FECHA_LIB_COLO', 'FECHA_LIB_IMP_TX', 'FECHA_LIB_IT', 'FECHA_LIB_APTO', 'ES_COLO', 'IT');
                //str_1 = nombres de las columnas
                //str_2 = valores de las columnas
                $str_1 = '';
                $str_2 = '';
                $coma = '';
                $sql_str = '';
                $col_limpia = '';
                
                //
                foreach($arreglo_col as $col){
                    if(isset($_POST[$col])){
                        $col_limpia = str_replace("'", "", $_POST[$col]);
                    }
                    $str_1 .= $coma . $col;
                    $str_2 .= $coma . "'" . $col_limpia . "'";
                    $coma = ', ';
                }
                $sql_str = 'INSERT INTO oocc.sitios_oomm( ' . $str_1 . ' ) VALUES (' . $str_2 . ')';
                
                 
                mysql_query($sql_str);
                include_once('envioMails.php');
                //Recorre todos los procesos hasta el primer estado que no esté liberado, 
               $proc_anterior = null;
                include_once 'logica.php';
                foreach(logica::$procesos as $proceso){
                    if($proceso['tipo'] == 'estado'){
                        if(isset($_POST[$proceso['nombre']]) && $_POST[$proceso['nombre']] != 'LIBERADO'){
                            break;
                        }
                        
                        $proc_anterior = $proceso;
                    }
                }
                //Si hay un elemento liberado, se manda un aviso de liberación.
                if($proc_anterior!=null)
                    mails::mandarMailLib($proc_anterior['nombre'], $_POST['COD_SITIO'], $_POST['TIPO_SITIO']);
            }
            
            //Se comprueba que los estados de liberación de cada proceso tengan sentido
            $dato = comprobarEstados();
            if(!$dato['valido']){
                echo '<script> alert("' . comprobarEstados()['mensaje'] . '")</script>';
            }else//si es así, se agrega
                agregarSitio();
        }
    
    ?>


						

<form method="post" id="AGREGA" action="#">
  <table border="1" align="center" style="border-collapse:collapse" width="50%">
    <tbody><tr>
      <td colspan="8" align="center" class="cabecera1">Agregar Sitio</td>
    </tr>
    
      <tr><td class="texto" width="10%">POP:</td>
            <input type="hidden" name="ES_COLO" id="COLO_HIDDEN" class="form-control" value="NO">
            <input type="hidden" name="COLOCALIZADOS" id="COLO" class="form-control" value="">

          <!--Estados-->
        <input type="hidden" name="APTO" id="APTO" class="form-control" style="width:98%" tabindex="1" value="LIBERADO">
        <input type="hidden" name="OOMM" id="OOMM" class="form-control" style="width:98%" tabindex="1" value="LIBERADO">
        <input type="hidden" name="IT" id="IT" class="form-control" style="width:98%" tabindex="1" value="LIBERADO">
          <!--Fechas-->
        <input type="hidden" name="FECHA_LIB_IT" id="IT" class="form-control" style="width:98%" tabindex="1" value="0000-00-00">
        <input type="hidden" name="FECHA_LIB_APTO"  class="form-control" style="width:98%" tabindex="1" value="0000-00-00">
          <input type="hidden" name="FECHA_LIB_OOMM"  class="form-control" style="width:98%" tabindex="1" value="0000-00-00">
        
      <td width="10%"><input type="text" name="COD_SITIO" id="COD_SITIO" class="form-control" style="width:98%" tabindex="3" value="">
      </td>
    
      <td class="texto" width="10%">Región:</td>
      <td colspan="6" width="10%"><select name="COD_REGION" id="REGION" style="width:98%"><option value="0">.::Seleccione Región::.</option><option value="1">Región de Tarapaca</option><option value="2">Región de Antofagasta</option><option value="3">Región de Atacama</option><option value="4">Región de Coquimbo</option><option value="5">Región de Valparaíso</option><option value="6">Región del Libertador Gral Bernardo OHiggins</option><option value="7">Región del Maule</option><option value="8">Región del Bio-Bio</option><option value="9">Región de la Araucanía</option><option value="10">Región de los Lagos</option><option value="11">Región Aysén del Gral Carlos Ibáñez del Campo</option><option value="12">Región de Magallanes y de la Antártica Chilena</option><option value="13">Región Metropolitana</option><option value="14">Region de los Rios</option><option value="15">Region de Arica y Parinacota</option></select>                        	
                  </td>
      
    </tr>
    <tr>
      <td class="texto" width="10%">Nombre:</td>
      <td width="10%"><input type="text" name="NOMBRE_SITIO" id="NOMBRE_SITIO" class="form-control" style="width:98%" tabindex="3" value="">
      <td>POP Anterior:</td>
        <td colspan="8" width="100%"><input type="text" name="COD_SITIO_REEMPLAZO" id="COD_SITIO_REEMPLAZO" class="form-control" style="width:98%" tabindex="3" value=""></td>
    </tr>
    <tr>
      <td class="texto" width="10%">Proyecto:</td>
      <td width="10%" colspan=""><select name="PROYECTO">
          <option value="">.::Seleccione Proyecto::.</option>
          <option value="CAPACIDAD" >COBERTURA</option>
          <option value="CAPACIDAD 2019">NUEVA COBERTURA</option>
          <option value="COBERTURA">CAPACIDAD</option>
        </select>
      </td>
	  <td class="texto" width="10%">Estado Proyecto:</td>
      <td width="10%" colspan="6">        
      			<select name="ESTADO_PROYECTO" style="width:98%">
                	<option value="">.::Estado Proyecto::.</option>
                    <option value="VIGENTE" selected>VIGENTE</option>
                    <option value="SUSPENDIDO">SUSPENDIDO</option>
                   
                </select>                             
      </td>
    </tr>
    <tr>
    <td class="texto" width="10%">Tipo Sitio:</td>
    	<td colspan="8">
            <select name="TIPO_SITIO" style="width:98%">
            	<option value="">.::Tipo Sitio::.</option>
                <option value="BAFI">BAFI</option>
                <option value="RANCO">RANCO</option>
            </select>                                              
        </td>
    </tr>
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>OOCC</b></td>
    </tr>
    <tr>
      <td class="texto" width="10%">Liberación OOCC:  </td>
      <td width="10%">
          <select name="LIBERACION_OOCC" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_OOCC', this.value)">
            <option value="LIBERADO">LIBERADO</option>
            <option value="NO LIBERADO" selected>NO LIBERADO</option>
            <option value="REPROCESO">REPROCESO</option>
          </select>                     
      </td>
      
      <td class="texto" width="2%">Comentario: </td>
        <td width="100%" colspan="3" ><textarea type="text"  name="COMENTARIO_OOCC" id="COMENTARIO_OOCC" class="form-control comentario" tabindex="10" value=""></textarea></td>
        <td class="texto" width="10%">Fecha Lib:</td>
        <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_OOCC" name="FECHA_LIB_ING_OOCC" value="0000-00-00" readonly></td>
    </tr>
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>OOEE</b></td>
    </tr>
    <tr> 
        <td class="texto" width="10%">Liberación OOEE: </td>
        <td width="10%">
          <select name="LIBERACION_OOEE" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_OOEE', this.value)">
              <option value="LIBERADO">LIIBERADO</option>
              <option value="NO LIBERADO" selected>NO LIBERADO</option>
              <option value="REPROCESO">REPROCESO</option>
            </select>   
        </td>
        <td class="texto" width="10%">Comentario: </td>
        <td width="10%" colspan="3">
            <textarea name="COMENTARIO_OOEE" id="CONDICION" class="form-control comentario" tabindex="10" value=""></textarea></td>
        <td class="texto" width="10%">Fecha Lib:</td>
        <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_OOEE" name="FECHA_LIB_ING_OOEE" value="0000-00-00" readonly></td>
        </tr>
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>COLOCALIZADO</b></td>
    </tr>
    <tr> 
        <td class="texto" width="10%">Colocalizado: <input type="checkbox" name="ES_COLOCALIZADO" id="ES_COLO" value="NO" onchange="colo()"></td>
        <td width="10%">
            <select name="COLOCALIZADOS" id="COLOCALIZADOS" style="width:100%" onchange="actualizarFecha('FECHA_LIB_COLO', this.value)">
              <option value="LIBERADO"  disabled>LIBERADO</option>
              <option value="NO LIBERADO" selected  disabled>NO LIBERADO</option>
              <option value="REPROCESO" disabled>REPROCESO</option>
            </select>
        </td>  
        <td class="texto" width="10%">Comentario: </td>
        <td width="10%" colspan="3">
            <textarea name="COMENTARIO_COLO" id="COMENTARIO_COLO" class="form-control comentario" tabindex="10" value=""></textarea>
        </td>        
        <td class="texto" width="10%">Fecha Lib:</td>
        <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_COLO" name="FECHA_LIB_COLO" value="0000-00-00" readonly></td>
    </tr>    
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Ingenieria RF</b></td>
    </tr>
    <tr> 
        <td class="texto" width="10%">ING RF:</td>
        <td width="10%">
            <select name="INGENIERIA_RF" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_RF', this.value)">
              <option value="LIBERADO">LIBERADO</option>
              <option value="NO LIBERADO" selected>NO LIBERADO</option>
              <option value="REPROCESO">REPROCESO</option>
            </select>
        </td>  
        <td colspan="4"></td>
        <td class="texto" width="10%">Fecha Lib:</td>
        <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_RF" name="FECHA_LIB_ING_RF" value="0000-00-00" readonly></td>
        
    </tr>    
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Ingenieria TX</b></td>
    </tr>
    <tr> 
        <td class="texto" width="10%">Ingeniería:</td>
        <td width="10%">
            <select name="INGENIERIA_TX" style="width:100%" onchange="actualizarFecha('FECHA_LIB_ING_TX', this.value)">
              <option value="LIBERADO">LIBERADO</option>
              <option value="NO LIBERADO" selected>NO LIBERADO</option>
              <option value="REPROCESO">REPROCESO</option>
            </select>
        </td>  
        <td colspan="4"></td>
        <td class="texto" width="10%">Fecha Lib:</td>
        <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_ING_TX" name="FECHA_LIB_ING_TX" value="0000-00-00" readonly></td>
        
    </tr>  
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Implementacion TX</b></td>
    </tr>
    <tr>
        <td class="texto" >Implementación:</td>
        <td width="10%">
            <select name="IMPLEMENTACION_TX" style="width:100%" onchange="actualizarFecha('FECHA_LIB_IMP_TX', this.value)">
              <option value="LIBERADO">LIBERADO</option>
              <option value="NO LIBERADO" selected>NO LIBERADO</option>
              <option value="REPROCESO">REPROCESO</option>
            </select>
        </td>     
        <td colspan="4"></td>
        <td class="texto" width="10%">Fecha Lib:</td>
        <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_IMP_TX" name="FECHA_LIB_IMP_TX" value="0000-00-00" readonly></td>
    </tr>
    <tr>
      <td colspan="8" class="textoc" bgcolor="#CCCCCC"><b>Implementación</b></td>
    </tr>
    <tr> 
        <td class="texto" width="10%">Imp:</td>
        <td width="10%">
            <select name="IMPLEMENTACION_NRO" style="width:100%" onchange="actualizarFecha('FECHA_LIB_IT', this.value)">
              <option value="LIBERADO">LIBERADO</option>
              <option value="NO LIBERADO" selected>NO LIBERADO</option>
              <option value="REPROCESO">REPROCESO</option>
            </select>
        </td>  
        <td colspan="4"></td>
        <td class="texto" width="10%">Fecha Lib:</td>
        <td width="10%"><input type="text" size="6" autocomplete="off" class="fecha" id="FECHA_LIB_IT" name="FECHA_LIB_IT" value="0000-00-00" readonly></td>
    </tr>    
    <tr>
      <td colspan="8" height="80px" align="center"><input type="submit" name="submit" value="[ Agregar_" style="background-color:#ff6702; font-family:'Trebuchet ms',Arial; font-size:13px; color:#FFF; width:125px; height:23px; display:inline-block;  border:0px;">
        &nbsp;
        <input type="button" name="Volver" value="[ Volver_" style="background-color:#ff6702; font-family:'Trebuchet ms',Arial; font-size:13px; color:#FFF; width:125px; height:23px; display:inline-block;  border:0px;" onclick="javascript:Pag_Anterior('');">
        <input type="button" name="limpiar" value="[ Limpiar_" style="background-color:#ff6702; font-family:'Trebuchet ms',Arial; font-size:13px; color:#FFF; width:125px; height:23px; display:inline-block;  border:0px;" onclick="javascript:actualizar('');"></td>
    </tr>
        
  </tbody></table>
</form>


</body></html>