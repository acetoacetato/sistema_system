
<?php
include_once ("../../../menu.php");

session_start();

if ( !isset($_SESSION['estado']) || $_SESSION['estado'] != "ok")
{
	header ("Location: ../../../login.php");
}


menu();
?>

<html>
    <head>
        <title>[ Usuarios_</title>
        
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../style.css">

        <!--Bootstrap-->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

        <link rel="stylesheet" type="text/css" href="../responsive.bootstrap.min.css">
        <!--DataTables-->
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
        
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        
        
        <!--Botones para DataTables-->
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        
    </head>

    <body>
    
        <div class="pre mx-auto table text-center" style="width: 95%; align: centered">
                <table id="tabla-usuarios" class="table table-bordered hover" align="center">
                    <thead align="center" class="thead" >
                        <tr><td colspan="28" class="cabecera">Listado de POP's</td></tr>
                        <tr>  
                            <!--
                                La clase none esconde la columna
                                La clase all muestra la columna
                            -->
                            <th class="all">Editar</th>
                            <th class="all">Usuario</th>
                            <th class="SELECCIONABLE all">Perfil</th> 
                            <th class="none">ID GDCI</th>
                            <th class="none">ID OOCC</th>
                        </tr>                        
                    </thead>
                        <tbody> </tbody>
                    <tfoot>
                            <th class="text-center">Editar</th>
                            <th class="text-center escribe" id="escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Usuario"/>
                            </th>
                            <th class="text-center select">Perfil</th>
                            <th class="text-center">ID GDCI</th>
                            <th class="text-center">ID OOCC</th>
                    </tfoot>
                </table>
            </div>
    
    </body>

    <script src="dataTable.js"></script>
</html>