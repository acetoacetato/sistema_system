
<?php
include_once ("../../menu.php");

session_start();

if ( !isset($_SESSION['estado']) || $_SESSION['estado'] != "ok")
{
	header ("Location: ../../login.php");
	header ("Location: ../../login.php");
}


menu();
?>
<!doctype HTML>


<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">

        <!--Bootstrap-->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <!--link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css"-->
        <link rel="stylesheet" type="text/css" href="responsive.bootstrap.min.css">
        <!--DataTables-->
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
        
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        
        
        <!--Botones para DataTables-->
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        
        <title>Liberaciones</title>
        
        
        <style>
            
            <?php 
                include_once('db.php');
                $usuario = $_SESSION['usuarioaux'];
                
                $perfil = obtenerPerfil($usuario);
                if($perfil == null || $perfil != 'Admin'){
            ?>
                .agregar{
                    display: none;
                }
            <?php
                }
            ?>

        
        </style>
    </head>
    <body>       
            <div class="pre mx-auto table text-center" style="width: 95%; align: centered">
                <table id="tabla-busqueda" class="table table-bordered hover" align="center">
                    <thead align="center" class="thead" >
                        <tr><td colspan="28" class="cabecera">Listado de POP's</td></tr>
                        <tr>
                            
                            <!--La clase EXPORTABLE incluye la columna en el excel a descargar
                                La clase none esconde la columna
                                La clase all muestra la columna
                                La clase SELECCIONABLE es para indicar que en dicha columna se busca con un select
                            -->
                            <th class="<?php  echo (($perfil == 'Control')? 'none':'all') ?> ">editar</th> 
                            <th class="all">detalles</th>
                            <th class="EXPORTABLE all">POP VIGENTE</th>
                            <th class="EXPORTABLE none">POP ANTIGUO</th>
                            <th class="EXPORTABLE all">NOMBRE SITIO</th>
                            <th class="SELECCIONABLE EXPORTABLE none">REGIÓN</th>
                            <th class="SELECCIONABLE EXPORTABLE all">TIPO SITIO</th>
                            <th class="SELECCIONABLE EXPORTABLE all">SUB PROYECTO</th>
                            <th class="SELECCIONABLE EXPORTABLE all">ESTADO</th>
                            <th class="SELECCIONABLE EXPORTABLE all">ES COLOCALIZADO</th>
                            <th class="SELECCIONABLE EXPORTABLE all">OOMM</th>
                            <th class="SELECCIONABLE EXPORTABLE all">APTO</th>
                            <th class="SELECCIONABLE EXPORTABLE all">Impl. NRO</th>
                            <th class="SELECCIONABLE EXPORTABLE all">Colo OK</th>
                            <th class="FECHA EXPORTABLE none">Fecha Lib Colo</th>
                            <th class="EXPORTABLE none">Comentario Colo</th>
                            <th class="SELECCIONABLE EXPORTABLE all">Ing. RF OK</th>
                            <th class="FECHA EXPORTABLE none">Fecha Lib RF</th>
                            <th class="SELECCIONABLE EXPORTABLE all">OOCC OK</th>
                            <th class="FECHA EXPORTABLE none">Fecha Lib. OOCC</th>
                            <th class="EXPORTABLE none">Comentario OOCC</th>
                            <th class="SELECCIONABLE EXPORTABLE all">OOEE OK</th>
                            <th class="FECHA EXPORTABLE none">Fecha Lib.OOEE</th>
                            <th class="EXPORTABLE none">Comentario OOEE</th>
                            <th class="SELECCIONABLE EXPORTABLE all">Ing TX OK</th>
                            <th class="FECHA EXPORTABLE none">Fecha Lib ING. TX</th>
                            <th class="SELECCIONABLE EXPORTABLE all">Impl TX OK</th>
                            <th class="FECHA EXPORTABLE none">Fecha lib Impl. TX</th>
                            <th class="FECHA none">Fecha IT</th>
                            <th class="FECHA EXPORTABLE none">Fecha OOMM</th>
                            <th class="FECHA EXPORTABLE none">Fecha APTO</th>
                            <th class="FECHA none">W_RF</th>
                            <th class="FECHA EXPORTABLE none">W_IT</th>
                            <th class="FECHA none">W_ING_TX</th>
                            <th class="FECHA none">W_IMP_TX</th>
                            <th class="FECHA none">W_OOCC</th>
                            <th class="FECHA none">W_OOEE</th>
                            <th class="FECHA none">W_COLO</th>
                            <th class="FECHA none">W_OOMM</th>
                            <th class="FECHA none">W_APTO</th>
                            
                        </tr>                        
                    </thead>
                        <tbody> </tbody>
                    <tfoot>
                        <!--tr-->
                            <th class="text-center">editar</th>
                            <th class="text-center">detalles</th>
                            <th class="text-center escribe" id="escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="POP VIGENTE"/>
                            </th>
                            <th class="text-center escribe" id="escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="POP ATIGUO"/>
                            </th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="NOMBRE SITIO"/>
                            </th>
                            <th class="text-center select">REGIÓN</th>
                            <th class="text-center select">TIPO SITIO</th>
                            <th class="text-center select">SUB PROYECTO</th>
                            <th class="text-center select">ESTADO</th>
                            <th class="text-center select">ES COLO</th>
                            <th class="text-center select">OOMM</th>
                            <th class="text-center select">APTO</th>
                            <th class="text-center select">Impl. NRO</th>
                        
                            <th class="text-center select">Colo OK</th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Fecha Lib. Colo"/>
                            </th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Comentario Colo"/>
                            </th>
                            <th class="text-center select">Ing. RF OK</th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Fecha Lib. RF"/>
                            </th>
                            <th class="text-center select">OOCC OK</th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Fecha Lib. OOCC"/>
                            </th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Comentario OOCC"/>
                            </th>
                            <th class="text-center select">OOEE OK</th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Fecha Lib."/>
                            </th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="4" title="Presiona [ENTER] para buscar" placeholder="Comentario OOEE"/>
                            </th>
                            
                            <th class="text-center select">Ing TX OK</th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="9" title="Presiona [ENTER] para buscar" placeholder="Fecha Lib."/>
                            </th>
                            <th class="text-center select">Impl TX OK</th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="9" title="Presiona [ENTER] para buscar" placeholder="Fecha Lib."/>
                            </th>
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="9" title="Presiona [ENTER] para buscar" placeholder="Fecha IT"/>
                            </th>
                        
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="9" title="Presiona [ENTER] para buscar" placeholder="Fecha OOMM"/>
                            </th>
                        
                            <th class="text-center escribe">
                                <input type="text"  class="escribe" size="9" title="Presiona [ENTER] para buscar" placeholder="Fecha APTO"/>
                            </th>
                        
                            <th class="text-center">W_RF</th>
                            <th class="text-center">W_IT</th>
                            <th class="text-center">W_ING_TX</th>
                            <th class="text-center">W_IMP_TX</th>
                            <th class="text-center">W_OOCC</th>
                            <th class="text-center">W_OOEE</th>
                            <th class="text-center">W_COLO</th>
                            <th class="text-center">W_OOMM</th>
                            <th class="text-center">W_APTO</th>
                            
                        <!--/tr-->   
                    </tfoot>
                </table>
            </div>
            
        <!--Creación de la tabla a través de dataTables-->
        <script type="text/javascript" src="dataTable.js"></script>

</body>
</html>
